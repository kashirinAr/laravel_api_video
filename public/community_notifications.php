<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

//get notifications for user 
if($choice == "0")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");   
    $data = array();    
    $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage,notification,type,createat from notifications left join patients on patients.id=notifications.senderid where notifications.receiverid=$userid ORDER BY notifications.id DESC";
    $result = $conn->query($sql);
    
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    $sql = "UPDATE notifications SET status = 1 WHERE receiverid = $userid";
    $conn->query($sql);
    $output = array('status' => 'true','data' => $data);
}
else if($choice == "1") // check new notifications
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");   
    $data = false;
    $sql = "select * from notifications where receiverid = $userid AND status = 0";            
    $result = $conn->query($sql);
    
    if($result->num_rows > 0)
    {
        $data = true;
    }
    
    $output = array('status' => 'true','data' => $data);
}
print(json_encode($output));

?>
