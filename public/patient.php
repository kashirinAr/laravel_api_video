<?php
    require_once('./include/config.php');
    
    $choice = $_POST['choice'];
    $output = array();
    // Update Schedule time
    if ($choice == "0")
    {
        $startTime = $_REQUEST['startTime'];
        $endTime = $_REQUEST['endTime'];
        $type = $_REQUEST['type'];
        $userid = $_REQUEST['userid'];

        $query = "";
        switch ($type)
        {
            case "0": // Morning
                $query = "UPDATE patients SET morning_start=$startTime, morning_end=$endTime WHERE id=$userid";
            break;
            case "1": // Midday
                $query = "UPDATE patients SET midday_start=$startTime, midday_end=$endTime WHERE id=$userid";
            break;
            case "2": // Evening
                $query = "UPDATE patients SET evening_start=$startTime, evening_end=$endTime WHERE id=$userid";
            break;
            case "3": // Night
                $query = "UPDATE patients SET night_start=$startTime, night_end=$endTime WHERE id=$userid";
            break;
        }
        $conn->query($query);
        $output = array('status' => 'true', 'message' => 'Succssfully updated');
    }
    // Get Schedule time
    else if ($choice == "1")
    {
        $userid = $_REQUEST['userid'];
        $query = "SELECT morning_start, morning_end, midday_start, midday_end, evening_start, evening_end, night_start, night_end FROM patients WHERE id=$userid";
        $result = $conn->query($query);
        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $output = array(
                    array("title" => "Morning", "timeStart" => $row["morning_start"], "timeEnd" => $row["morning_end"], "image" => "Morning Icon"),
                    array("title" => "Midday", "timeStart" => $row["midday_start"], "timeEnd" => $row["midday_end"], "image" => "Midday Icon"),
                    array("title" => "Evening", "timeStart" => $row["evening_start"], "timeEnd" => $row["evening_end"], "image" => "Evening Icon"),
                    array("title" => "Night", "timeStart" => $row["night_start"], "timeEnd" => $row["night_end"], "image" => "Night Icon")                    
                );
            }
        }        
    }
    // Update Device Token and Timezone after sign-up
    else if ($choice == "2")
    {
        $userid = $_REQUEST['userid'];
        $timezone = $_REQUEST['timezone'];
        $deviceToken = $_REQUEST['deviceToken'];
        
        $query = "UPDATE patients SET timezone='$timezone', deviceToken='$deviceToken' WHERE id=$userid";
        $conn->query($query);
        $output = array('status' => 'true', 'message' => 'Successfully updated');
    }
    // Set Snooze time
    else if ($choice == "3")
    {
        $userid = $_REQUEST['userid'];
        $snooze = $_REQUEST['snooze'];

        $query = "UPDATE patients SET snooze = $snooze WHERE id=$userid";
        $conn->query($query);
        $output = array('status' => 'true', 'message' => 'Successfully updated');
    }
    // Get snooze time
    else if ($choice == "4")
    {
        $userid = $_REQUEST['userid'];
        $query = "SELECT snooze FROM patients WHERE id=$userid";

        $result = $conn->query($query);
        if ($result->num_rows > 0)
        {
            while ($row = $result->fetch_assoc())
            {
                $output = array('status' => 'true', 'snooze' => $row['snooze']);
            }
        }
    }

    echo json_encode($output);
?>