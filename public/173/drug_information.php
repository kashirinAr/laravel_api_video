<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

//insert drug
if($choice == "0")
{
    $directions = ((!empty($_REQUEST['directions'])) ? $_REQUEST['directions'] : "");
    $dose = ((!empty($_REQUEST['dose'])) ? $_REQUEST['dose'] : "");
    $quantity = ((!empty($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : "");
    $price = ((!empty($_REQUEST['price'])) ? $_REQUEST['price'] : "");
    $lowest_price = ((!empty($_REQUEST['lowest_price'])) ? $_REQUEST['lowest_price'] : "");
    $cheapest_price = ((!empty($_REQUEST['cheapest_price'])) ? $_REQUEST['cheapest_price'] : "");
    $comparison_price = ((!empty($_REQUEST['comparison_price'])) ? $_REQUEST['comparison_price'] : "");
    $expiration = ((!empty($_REQUEST['expiration'])) ? $_REQUEST['expiration'] : "");
    $manufacture = ((!empty($_REQUEST['manufacture'])) ? $_REQUEST['manufacture'] : "");
    $substituted = ((!empty($_REQUEST['substituted'])) ? $_REQUEST['substituted'] : "");
    $filed_date = ((!empty($_REQUEST['filed_date'])) ? $_REQUEST['filed_date'] : "");
    $warnings = ((!empty($_REQUEST['warnings'])) ? $_REQUEST['warnings'] : "");
    $type = ((!empty($_REQUEST['type'])) ? $_REQUEST['type'] : "tablet");

    if (!empty($_FILES['drug_image']['name'])) {
        //$tmp_name = $_FILES["drug_image"]["tmp_name"];
        $names = $_FILES["drug_image"]["name"];
        $image_name = date('dmYhis') . $names;
        //$image = 'BASE_URL' . $image_name;
        $uploadpath = "drugimage/" . $image_name;
        move_uploaded_file($_FILES["drug_image"]["tmp_name"], $uploadpath);

    }
    else {
        $image_name = '';
    }

    $sql = "insert into medications(directions,dose,quantity,price,lowest_price,cheapest_price,comparison_price,expiration,manufacture,substituted,filed_date,warnings,type,image) 
        values('$directions','$dose',$quantity,$price,$lowest_price,$cheapest_price,$comparison_price,'$expiration','$manufacture','$substituted','$filed_date','$warnings','$type','$image_name')";

    $conn->query($sql);

    $output = array('status' => 'true','message' => "Successfully registered");

}
//get drugs
else if($choice == "1")
{
    echo "d";
    $info = array();
    $result = $conn->query("select * from medications");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $info);
}
//update drug
else if($choice == "2")
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $directions = ((!empty($_REQUEST['directions'])) ? $_REQUEST['directions'] : "");
    $dose = ((!empty($_REQUEST['dose'])) ? $_REQUEST['dose'] : "");
    $quantity = ((!empty($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : "");
    $price = ((!empty($_REQUEST['price'])) ? $_REQUEST['price'] : "");
    $lowest_price = ((!empty($_REQUEST['lowest_price'])) ? $_REQUEST['lowest_price'] : "");
    $cheapest_price = ((!empty($_REQUEST['cheapest_price'])) ? $_REQUEST['cheapest_price'] : "");
    $comparison_price = ((!empty($_REQUEST['comparison_price'])) ? $_REQUEST['comparison_price'] : "");
    $expiration = ((!empty($_REQUEST['expiration'])) ? $_REQUEST['expiration'] : "");
    $manufacture = ((!empty($_REQUEST['manufacture'])) ? $_REQUEST['manufacture'] : "");
    $substituted = ((!empty($_REQUEST['substituted'])) ? $_REQUEST['substituted'] : "");
    $filed_date = ((!empty($_REQUEST['filed_date'])) ? $_REQUEST['filed_date'] : "");
    $warnings = ((!empty($_REQUEST['warnings'])) ? $_REQUEST['warnings'] : "");
    $type = ((!empty($_REQUEST['type'])) ? $_REQUEST['type'] : "tablet");

    if (!empty($_FILES['drug_image']['name'])) {
        //$tmp_name = $_FILES["drug_image"]["tmp_name"];
        $names = $_FILES["drug_image"]["name"];
        $image_name = date('dmYhis') . $names;
        //$image = 'BASE_URL' . $image_name;
        $uploadpath = "drugimage/" . $image_name;
        move_uploaded_file($_FILES["drug_image"]["tmp_name"], $uploadpath);

        $sql = "update medications set directions='$directions',dose='$dose',quantity=$quantity,price=$price,lowest_price=$lowest_price,cheapest_price=$cheapest_price,
            comparison_price=$comparison_price,expiration='$expiration',manufacture='$manufacture',substituted='$substituted',filed_date='$filed_date',warnings='$warnings',type='$type' 
            , image='$image_name' where id=$drugid";
    }
    else {
        $sql = "update medications set directions='$directions',dose='$dose',quantity=$quantity,price=$price,lowest_price=$lowest_price,cheapest_price=$cheapest_price,
            comparison_price=$comparison_price,expiration='$expiration',manufacture='$manufacture',substituted='$substituted',filed_date='$filed_date',warnings='$warnings',type='$type' 
            where id=$drugid";
    }
    $conn->query($sql);
    $output = array('status' => 'true','message' => 'Successfully updated.');

}
//delete drug
else if($choice == "3")
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $conn->query("delete from medications where id=$drugid");
    $output = array('status' => 'true','message' => 'Successfully deleted.');
}
//get detail drug
else if($choice == "4")
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $result = $conn->query("select * from medications where id=$drugid");
    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $output = array('status' => 'true','data' => $row);
    }
    else{
        $output = array('status' => 'false','message' => "No data.");
    }
}
print(json_encode($output));

?>
