<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

//get notifications for user 
if($choice == "0")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");   
    $send = ((!empty($_REQUEST['send'])) ? $_REQUEST['send'] : "");
    $receiverid = ((!empty($_REQUEST['receiverid'])) ? $_REQUEST['receiverid'] : "");
    $data = array();
    if($send == "receive")
        $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage,notification,createat from notifications left join patients on patients.id=notifications.senderid where notifications.receiverid=$userid";
    else
        $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage,notification,createat from notifications left join patients on patients.id=notifications.receiverid where notifications.senderid=$userid and notifications.receiverid=$receiverid";
    $result = $conn->query($sql);
    
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    
    $output = array('status' => 'true','data' => $data);
}
print(json_encode($output));

?>
