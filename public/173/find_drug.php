<?php
include './include/config.php';

$resultSet = [];
$dbcon = mysqli_connect($servername, $username, $password, "db_ndcd") or die('Could not connect: ' . mysqli_connect_error());


if (!is_null($_POST["NDCNumber"])) {
	getMedicineByNDC();
}
else {
	if (!is_null($_POST["Manufacturer"]) && !is_null($_POST["DrugName"]) && !is_null($_POST["DrugForm"]) && !is_null($_POST["Dosage"])) {
		getMedicineByOther();
	}
}

function prepareQuery($name, $form, $dosage, $labeler) {
	$query = "SELECT PROPRIETARYNAME, ACTIVE_NUMERATOR_STRENGTH, ACTIVE_INGRED_UNIT, DOSAGEFORMNAME, LABELERNAME, PRODUCTNDC FROM Products WHERE PROPRIETARYNAME LIKE '$name%' AND LABELERNAME LIKE '$labeler%' AND DOSAGEFORMNAME LIKE '$form%'";
	preg_match_all("![1-9].?[0-9]*!", $dosage, $numerators);
	preg_match_all("![A-Za-z]+!", $dosage, $units);
	foreach ($numerators[0] as $numerator) {
		$query = $query . "AND ACTIVE_NUMERATOR_STRENGTH REGEXP '\[\[:<:\]\]$numerator\[\[:>:\]\]'";
	}
	foreach ($units[0] as $unit) {
		$query = $query . "AND ACTIVE_INGRED_UNIT REGEXP '\[\[:<:\]\]$unit\[\[:>:\]\]'";
	}
	return $query;
}

function getMedicineByOther() {
	global $dbcon;
	$call = mysqli_prepare($dbcon, "");
	$labeler = $_POST["Manufacturer"];
	$name = $_POST["DrugName"];
	$form = rtrim($_POST["DrugForm"], "sS");
	$query = prepareQuery($name, $form, $_POST["Dosage"], $labeler);
	
	if ($result = mysqli_query($dbcon, $query) and mysqli_num_rows($result) > 0) {
		$first_result = mysqli_fetch_assoc($result);
		$output = [];
		$output["name"] = $first_result["PROPRIETARYNAME"];
		$output["form"] =  $first_result["DOSAGEFORMNAME"];

		$dosage = "";
		$numerators = explode("; ", $first_result["ACTIVE_NUMERATOR_STRENGTH"]);
		$units = explode("; ", $first_result["ACTIVE_INGRED_UNIT"]);
		for ($i = 0; $i < count($numerators); $i++) {
			$dosage = $dosage . $numerators[$i] . $units[$i] . "-";
		}
		$output["dosage"] = rtrim($dosage, "-");
		$output["manufacturer"] = $first_result["LABELERNAME"];
		$output["ndc"] = $first_result["PRODUCTNDC"];
		
		echo json_encode($output);
	    mysqli_free_result($result);
	}
	else {
		if ($name = strrpos($name, " ") !== false) {
			$name = substr($name, 0, strrpos($name, " ") - 1) ;
			$query = prepareQuery($name, $form, $_POST["Dosage"], $labeler);
			if ($result = mysqli_query($dbcon, $query)) {
				$first_result = mysqli_fetch_assoc($result);
				$output = [];
				$output["name"] = $first_result["PROPRIETARYNAME"];
				$output["form"] =  $first_result["DOSAGEFORMNAME"];

				$dosage = "";
				$numerators = explode("; ", $first_result["ACTIVE_NUMERATOR_STRENGTH"]);
				$units = explode("; ", $first_result["ACTIVE_INGRED_UNIT"]);
				for ($i = 0; $i < count($numerators); $i++) {
					$dosage = $dosage . $numerators[$i] . $units[$i] . "-";
				}
				$output["dosage"] = rtrim($dosage, "-");
				$output["manufacturer"] = $first_result["LABELERNAME"];
				$output["ndc"] = $first_result["PRODUCTNDC"];
		
				echo json_encode($output);
			    mysqli_free_result($result);
			}
		}
	}
}

function getMedicineByNDC() {
	global $dbcon;
	$NDCNumber = "";
	if (strpos($_POST["NDCNumber"], '-') === false) {
		$NDCNumber = substr($_POST["NDCNumber"], 0, 5) . "-" . substr($_POST["NDCNumber"], 5,4);
	}
	else {
		$NDCNumber = $_POST["NDCNumber"];
	}
	$call = mysqli_prepare($dbcon, "CALL getMedicineByNDC(?)");
	mysqli_stmt_bind_param($call, "s", $NDCNumber);
	mysqli_stmt_execute($call);
	mysqli_stmt_bind_result($call, $name, $numerator, $unit, $form, $labeler, $ndc);

	if ($r = mysqli_stmt_fetch($call)) {
		$resultSet['name'] = $name;
		$numerators = explode("; ", $numerator);
		$units = explode("; ", $unit);
		$dosage = "";
		for ($i = 0; $i < count($numerators); $i++) {
			$dosage = $dosage . $numerators[$i] . $units[$i] . "-";
		}
		$resultSet['dosage'] = rtrim($dosage, "-");
		$resultSet['form'] = $form;
		$resultSet['manufacturer'] = $labeler;
		$resultSet['ndc'] = $ndc;
		echo json_encode($resultSet);
	}
	else {
		mysqli_stmt_close($call);
		if ($NDCNumber[6] == "0" && $NDCNumber[5] == "-") {
			$NDCNumber = substr($NDCNumber, 0, 6) . substr($NDCNumber, 7);
		}
		elseif ($NDCNumber[0] == "0") {
			$NDCNumber = substr($NDCNumber, 1);
		}

		$call = mysqli_prepare($dbcon, "CALL getMedicineByNDC(?)");
		mysqli_stmt_bind_param($call, "s", $NDCNumber);
		mysqli_stmt_execute($call);
		mysqli_stmt_bind_result($call, $name, $numerator, $unit, $form, $labeler, $ndc);
		if ($r = mysqli_stmt_fetch($call)) {
			$resultSet['name'] = $name;
			$numerators = explode("; ", $numerator);
			$units = explode("; ", $unit);
			$dosage = "";
			for ($i = 0; $i < count($numerators); $i++) {
				$dosage = $dosage . $numerators[$i] . $units[$i] . "-";
			}
			$resultSet['dosage'] = rtrim($dosage, "-");
			$resultSet['form'] = $form;
			$resultSet['manufacturer'] = $labeler;
			$resultSet['ndc'] = $ndc;
			echo json_encode($resultSet);
		}
	}
}
mysqli_close($dbcon);
?>

