<?php
require_once('./include/config.php');



// Get device tokens per user
$sql = "SELECT A.id, GROUP_CONCAT(B.taketime) as takeTimes, A.deviceToken, A.timezone FROM patients A LEFT JOIN mymedications B ON A.id=B.userid GROUP BY A.id";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $times = explode(",", $row["takeTimes"]);
        
        $date = new DateTime();
        $timezone = $date->getTimezone();
        
        if ($row["timezone"] != "") {
            $timezone = new DateTimeZone($row["timezone"]);
        }
        $currentTime = new DateTime("now", $timezone);

        foreach($times as $time) {  
            if ($time != "") {
                $time = str_replace(' ', '', $time);
                $take_hour = substr($time, 0, 2);
                $take_min = substr($time, 2, 2);
                $take_time = date_create($take_hour.":".$take_min, $timezone);
                $diff = $take_time->diff($currentTime);
                $minutes = (int)($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
                
                if ($currentTime < $take_time && $minutes == 10)
                {
                    echo "sendMessage...";
                    sendAPNS($row["deviceToken"]);
                }
            }
        }
    }
}

function sendAPNS($deviceToken) {
    /* We are using the sandbox version of the APNS for development. For production
    environments, change this to ssl://gateway.push.apple.com:2195 */
    $apnsServer = 'ssl://gateway.sandbox.push.apple.com:2195';
    /* Make sure this is set to the password that you set for your private key
    when you exported it to the .pem file using openssl on your OS X */
    $privateKeyPassword = '';
    /* Put your own message here if you want to */
    $message = "It's time to take your medication";
    
    /* Replace this with the name of the file that you have placed by your PHP
    script file, containing your private key and certificate that you generated
    earlier */
    $pushCertAndKeyPemFile = __DIR__.'/apns.pem';
    
    $stream = stream_context_create();
    // stream_context_set_option($stream,'ssl','passphrase',$privateKeyPassword);
    stream_context_set_option($stream,'ssl','local_cert',$pushCertAndKeyPemFile);

    $connectionTimeout = 20;
    $connectionType = STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT;
    $connection = stream_socket_client($apnsServer,$errorNumber,$errorString,$connectionTimeout,$connectionType,$stream);

    if (!$connection){
        echo "Failed to connect to the APNS server. Error no = $errorNumber<br/>";
        exit;
    } else {
        echo "Successfully connected to the APNS. Processing...</br>";
    }
    
    $messageBody['aps'] = array('alert' => $message,'sound' => 'default','badge' => 2,);
    $payload = json_encode($messageBody);
    
    $notification = chr(0) .
    pack('n', 32) .
    pack('H*', $deviceToken) .
    pack('n', strlen($payload)) .
    $payload;

    $wroteSuccessfully = fwrite($connection, $notification, strlen($notification));
    if (!$wroteSuccessfully){
        echo "Could not send the message<br/>";
    }
    else {
        echo "Successfully sent the message<br/>";
    }
    fclose($connection);
}

function create_payload_json($message) {
    //Badge icon to show at users ios app icon after receiving notification
    $badge = "0";
    $sound = 'default';

    $payload = array();
    $payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound);
    return json_encode($payload);
}

?>
