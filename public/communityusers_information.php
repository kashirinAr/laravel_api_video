<?php
require_once('./include/config.php');

$choice = $_POST['choice'];
//get members to follow me
if($choice == "0")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");   
    $data = array();
    $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage from communityusers left join patients on patients.id=communityusers.friendId where communityusers.userId=$userid and communityusers.state=1";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $data);
}
//add community user
else if($choice == "2")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $usercode = ((!empty($_REQUEST['usercode'])) ? $_REQUEST['usercode'] : "");
    
    $sql = "select * from patients where usercode='$usercode'";
    $result = $conn->query($sql);
    if($result->num_rows == 0)
    {
        $output = array('status' => 'false','message' => "Can not find code.");
    }
    else {
        $row = $result->fetch_assoc();

        if($row['id'] == $userid)
        {
            $output = array('status' => 'false','message' => "You  can not add yourself.");
        }
        else {
            $microtime = microtime();
            $comps = explode(' ', $microtime);
            $value = sprintf('%d%03d', $comps[1], $comps[0] * 1000);
            $value = substr($value,0,10);
            $currentcode = $value;

            if ((int)$usercode + 5 * 60 > (int)$currentcode)
            {
                $output = array('status' => 'false','message' => "Your code was expired.");
            }
            else {
                $receiverid = $row['id'];
                $sql = "select * from communityusers where userId=$userid and friendId=$receiverid and state=1";
                $result = $conn->query($sql);
                if ($result->num_rows > 0)
                {
                    $output = array('status' => 'false','message' => "You have added the user already.");
                }
                else {
                    $sql = "insert into communityusers(userId,friendId,state) values($userid,$receiverid,1)";
                    $conn->query($sql);

                    $data = array();
                    $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage from communityusers left join patients on patients.id=communityusers.receiverid where communityusers.userId=$userid and communityusers.state=1";
                    $result = $conn->query($sql);
                    if($result->num_rows > 0)
                    {
                        while($row = $result->fetch_assoc())
                        {
                            $data[] = $row;
                        }
                    }
                    $output = array('status' => 'true','data' => $data);                    
                }
            }
        }
    }   
}
//get usercode.
else if($choice == "4")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $microtime = microtime();
    $comps = explode(' ', $microtime);
    // Note: Using a string here to prevent loss of precision
    // in case of "overflow" (PHP converts it to a double)
    //echo sprintf('%d%03d', $comps[1], $comps[0] * 1000);
    $value = sprintf('%d%03d', $comps[1], $comps[0] * 1000);
    $value = substr($value,0,5);
    $usercode = $value;
    $sql = "update patients set usercode='$usercode' where id=$userid";
    $conn->query($sql);
    $output = array('status' => 'true','usercode' => $usercode);
}
else if($choice == "5") // chedk validate usercode
{
    $code = ((!empty($_REQUEST['code'])) ? $_REQUEST['code'] : "");
    $sql = "select patients.id from patients where usercode = '$code'";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
        $output = array('status' => 'true');
    else
        $output = array('status' => 'false');
}

print(json_encode($output));

?>