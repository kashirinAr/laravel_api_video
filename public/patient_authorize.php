<?php
    require_once('./include/config.php');
    
    $choice = $_POST['choice'];
    global $output;
    //sign up
    if($choice == "0")
    {
        $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
        
        $password = ((!empty($_REQUEST['password'])) ? $_REQUEST['password'] : "");
        $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
        $result = $conn->query("select * from patients where email='$email'");
        if($result->num_rows > 0)
        {
            $output = array('status' => 'false','message' => "Your email is in use");
        }
        else
        {
            $date = date('Y-m-d H:i:s');
            $usertype = ((!empty($_REQUEST['usertype'])) ? $_REQUEST['usertype'] : "patient");
            $language = ((!empty($_REQUEST['language'])) ? $_REQUEST['language'] : "");
            $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
            $street = ((!empty($_REQUEST['street'])) ? $_REQUEST['street'] : "");
            $city = ((!empty($_REQUEST['city'])) ? $_REQUEST['city'] : "");
            $state = ((!empty($_REQUEST['state'])) ? $_REQUEST['state'] : "");
            $zipcode = ((!empty($_REQUEST['zipcode'])) ? $_REQUEST['zipcode'] : "");
            $first_name = ((!empty($_REQUEST['first_name'])) ? $_REQUEST['first_name'] : "");
            $last_name = ((!empty($_REQUEST['last_name'])) ? $_REQUEST['last_name'] : "");
            $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
            $birthday = ((!empty($_REQUEST['birthday'])) ? $_REQUEST['birthday'] : "");
            $age = ((!empty($_REQUEST['age'])) ? $_REQUEST['age'] : "0");
            $diagnosis_date = ((!empty($_REQUEST['diagnosis_date'])) ? $_REQUEST['diagnosis_date'] : "");
            $application_location = ((!empty($_REQUEST['application_location'])) ? $_REQUEST['application_location'] : "");
            $location = ((!empty($_REQUEST['location'])) ? $_REQUEST['location'] : "");
            $sex = ((!empty($_REQUEST['sex'])) ? $_REQUEST['sex'] : "male");
            $timezone = ((!empty($_REQUEST['timezone'])) ? $_REQUEST['timezone'] : "");
            $deviceToken = ((!empty($_REQUEST['deviceToken'])) ? $_REQUEST['deviceToken'] : "");

            $hash = password_hash($password,PASSWORD_DEFAULT);
            
            $sql = "insert into patients(usertype,language,phone_number,email,password,street,city,state,zipcode,first_name,last_name,user_name,birthday,age,diagnosis_date,application_location,location, sex, timezone, deviceToken) values('$usertype','$language','$phone_number','$email','$hash','$street','$city','$state','$zipcode','$first_name','$last_name','$user_name','$birthday', $age,'$diagnosis_date','$application_location','$location','$sex', '$timezone', '$deviceToken')";
            
            $conn->query($sql);
            
            $last = $conn->insert_id;
            
            $result = $conn->query("select * from patients where id=$last");
            
            if ($result->num_rows > 0)
            {
                $row = $result->fetch_assoc();
                $row['password'] = "";
                $userinfo = $row;
                $output = array('status' => 'true','message' => 'You have signup successfully','userinfo' => $userinfo);
            }            
        }
    }
    //login
    else if($choice == "1")
    {
        $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
        $password = ((!empty($_REQUEST['password'])) ? $_REQUEST['password'] : "");
        $timezone = ((!empty($_REQUEST['timezone'])) ? $_REQUEST['timezone'] : "");
        $deviceToken = ((!empty($_REQUEST['deviceToken'])) ? $_REQUEST['deviceToken'] : "");
        
        $sql = "select * from patients where email='$email'";
        
        $result = $conn->query($sql);
        $userinfo = array();
        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                if (password_verify($password, $row['password'])) {
                    $sql = "update patients set timezone='$timezone', deviceToken='$deviceToken' where email='$email'";
                    $conn->query($sql);
                    // Success!
                    //echo "success";
                    $row['password'] = "";
                    $userinfo = $row;
                    $output = array('status' => 'true','userinfo' => $userinfo);
                }
                else {
                    // Invalid credentials
                    //echo "failure";
                    $output = array('status' => 'false','message' => "Invalid email or password");
                }
            }
        }
        else
        {
            $output = array('status' => 'false','message' => "Please check your email Or password");
        }
    }
    
    //update user
    else if($choice == "2")
    {
        $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
        $date = date('Y-m-d H:i:s');
        $usertype = ((!empty($_REQUEST['usertype'])) ? $_REQUEST['usertype'] : "patient");
        $language = ((!empty($_REQUEST['language'])) ? $_REQUEST['language'] : "");
        $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
        $street = ((!empty($_REQUEST['street'])) ? $_REQUEST['street'] : "");
        $state = ((!empty($_REQUEST['state'])) ? $_REQUEST['state'] : "");
        $city = ((!empty($_REQUEST['city'])) ? $_REQUEST['city'] : "");
        $zipcode = ((!empty($_REQUEST['zipcode'])) ? $_REQUEST['zipcode'] : "");
        $first_name = ((!empty($_REQUEST['first_name'])) ? $_REQUEST['first_name'] : "");
        $last_name = ((!empty($_REQUEST['last_name'])) ? $_REQUEST['last_name'] : "");
        $birthday = ((!empty($_REQUEST['birthday'])) ? $_REQUEST['birthday'] : "");
        $age = ((!empty($_REQUEST['age'])) ? $_REQUEST['age'] : "0");
        $diagnosis_date = ((!empty($_REQUEST['diagnosis_date'])) ? $_REQUEST['diagnosis_date'] : "");
        $application_location = ((!empty($_REQUEST['application_location'])) ? $_REQUEST['application_location'] : "");
        $location = ((!empty($_REQUEST['location'])) ? $_REQUEST['location'] : "");
        $sex = ((!empty($_REQUEST['sex'])) ? $_REQUEST['sex'] : "male");
        
        $result = $conn->query("select * from patients where id=$userid");
        if($result->num_rows > 0)
        {
            
            $conn->query("update patients set usertype='$usertype',language='$language',phone_number='$phone_number',street='$street',city='$city',zipcode='$zipcode',first_name='$first_name',
                         last_name='$last_name',birthday='$birthday',age=$age,diagnosis_date='$diagnosis_date',application_location='$application_location',location='$location',sex='$sex',updated_at='$date',state='$state' where id=$userid");
                         $output = array('status' => 'true','message'=>'Successfully updated');
                         }
                         else {
                         $output = array('status' => 'false','message'=>'Userid does not exist');
                         }
                         
                         }
                         //change password
                         else if($choice == "3")
                         {
                         $currentpassword = ((!empty($_REQUEST['currentpassword'])) ? $_REQUEST['currentpassword'] : "");
                         $newpassword = ((!empty($_REQUEST['newpassword'])) ? $_REQUEST['newpassword'] : "");
                         $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
                         
                         $result = $conn->query("select * from patients where email='$email'");
                         
                         if($result->num_rows > 0)
                         {
                         while($row = $result->fetch_assoc())
                         {
                         if (password_verify($currentpassword, $row['password'])) {
                         
                         $hash = password_hash($newpassword,PASSWORD_DEFAULT);
                         
                         $conn->query("update patients set password='$hash' where email='$email'");
                         
                         $output = array('status' => 'true','message' => 'Password changed successfully.');
                         }
                         else {
                         // Invalid credentials
                         //echo "failure";
                         $output = array('status' => 'false','message' => "Invalid current password");
                         }
                         }
                         
                         }
                         else
                         {
                         $output = array('status' => 'false','message' => "Please check your email");
                         }
                         }
                         //changeEmail
                         else if($choice == "8")
                         {
                         $currentemail = ((!empty($_REQUEST['currentemail'])) ? $_REQUEST['currentemail'] : "");
                         $newemail= ((!empty($_REQUEST['newemail'])) ? $_REQUEST['newemail'] : "");
                         $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
                         $result = $conn->query("SELECT * FROM `patients` WHERE email = '$email'");
                         if($result->num_rows > 0)
                         {
                         while($row = $result->fetch_assoc())
                         {
                         
                         $l =  strcmp( $currentemail, $row['email']);
                         if ($l == 0){
                         $conn->query("update patients set email='$newemail' where email='$email'");
                         $output = array('status' => 'true','message' => 'email changed successfully.');
                         }else{
                          $output = array('status' => 'false','message' => "email not mathces.");
                          }
                         }

                         }
                         else
                         {
                         $output = array('status' => 'false','message' => "Please check your email");
                         }

                         }
                         
                         //user inactive
                         else if($choice == "4")
                         {
                         $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
                         $result = $conn->query("select * from patients where id=$userid");
                         if($result->num_rows > 0)
                         {
                         
                         $conn->query("update patients set active='0'where id=$userid");
                         $output = array('status' => 'true','message'=>'Successfully inactived');
                         }
                         else {
                         $output = array('status' => 'false','message'=>'Userid does not exist');
                         }
                         
                         }
                         //community user sign up
                         else if($choice == "5")
                         {
                         $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
                         
                         $password = ((!empty($_REQUEST['password'])) ? $_REQUEST['password'] : "");
                         $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
                         
                         
                         $result = $conn->query("select * from patients where email='$email'");
                         if($result->num_rows > 0)
                         {
                         $output = array('status' => 'false','message' => "Your email is in use");
                         }
                         else
                         {
                         $date = date('Y-m-d H:i:s');
                         $usertype = ((!empty($_REQUEST['usertype'])) ? $_REQUEST['usertype'] : "patient");
                         $language = ((!empty($_REQUEST['language'])) ? $_REQUEST['language'] : "");
                         $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
                         $street = ((!empty($_REQUEST['street'])) ? $_REQUEST['street'] : "");
                         $city = ((!empty($_REQUEST['city'])) ? $_REQUEST['city'] : "");
                         $state = ((!empty($_REQUEST['state'])) ? $_REQUEST['state'] : "");
                         $zipcode = ((!empty($_REQUEST['zipcode'])) ? $_REQUEST['zipcode'] : "");
                         $first_name = ((!empty($_REQUEST['first_name'])) ? $_REQUEST['first_name'] : "");
                         $last_name = ((!empty($_REQUEST['last_name'])) ? $_REQUEST['last_name'] : "");
                         $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
                         $birthday = ((!empty($_REQUEST['birthday'])) ? $_REQUEST['birthday'] : "");
                         $age = ((!empty($_REQUEST['age'])) ? $_REQUEST['age'] : "0");
                         $diagnosis_date = ((!empty($_REQUEST['diagnosis_date'])) ? $_REQUEST['diagnosis_date'] : "");
                         $application_location = ((!empty($_REQUEST['application_location'])) ? $_REQUEST['application_location'] : "");
                         $location = ((!empty($_REQUEST['location'])) ? $_REQUEST['location'] : "");
                         $sex = ((!empty($_REQUEST['sex'])) ? $_REQUEST['sex'] : "male");
                         
                         $microtime = microtime();
                         $comps = explode(' ', $microtime);
                         // Note: Using a string here to prevent loss of precision
                         // in case of "overflow" (PHP converts it to a double)
                         //echo sprintf('%d%03d', $comps[1], $comps[0] * 1000);
                         $value = sprintf('%d%03d', $comps[1], $comps[0] * 1000);
                         $value = substr($value,0,10);
                         $usercode = $value;//uniqid();
                         
                         $hash = password_hash($password,PASSWORD_DEFAULT);
                         $name = "name";
                         if (!empty($_FILES[$name]['name'])) {
                         
                         $names = $_FILES[$name]["name"];
                         $names = preg_replace('/\s+/', '', $names);
                         $banner = date('dmYhis') . $names;
                         $uploadpath = "./upload/profile/" . $banner;
                         move_uploaded_file($_FILES[$name]["tmp_name"], $uploadpath);
                         
                         $sql = "insert into patients(usertype,language,phone_number,email,password,street,city,state,zipcode,first_name,last_name,user_name,birthday,age,diagnosis_date,application_location,location,
                         sex,usercode,userimage) values('$usertype','$language','$phone_number','$email','$hash','$street','$city','$state','$zipcode','$first_name','$last_name','$user_name','$birthday',
                                                        $age,'$diagnosis_date','$application_location','$location','$sex','$usercode','$banner')";
            
            $conn->query($sql);
            
            $last = $conn->insert_id;
            
            $result = $conn->query("select * from patients where id=$last");
            
            if ($result->num_rows > 0)
            {
                $row = $result->fetch_assoc();
                $row['password'] = "";
                $userinfo = $row;
                $output = array('status' => 'true','message' => 'You have signup successfully','userinfo' => $userinfo);
            }
            else
                $output = array('status' => 'false','message' => 'File upload failure');
        }
        else
        {
            $output = array('status' => 'false','message' => 'File upload failure');
        }
    }
    }
    //community user image update
    else if($choice == "6")
    {
        $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
        // Note: Using a string here to prevent loss of precision
        // in case of "overflow" (PHP converts it to a double)
        //echo sprintf('%d%03d', $comps[1], $comps[0] * 1000);
        $name = "name";
        if (!empty($_FILES[$name]['name'])) {
            
            $names = $_FILES[$name]["name"];
            $names = preg_replace('/\s+/', '', $names);
            $banner = date('dmYhis') . $names;
            $uploadpath = "./upload/profile/" . $banner;
            move_uploaded_file($_FILES[$name]["tmp_name"], $uploadpath);
            
            echo $sql = "update patients set userimage='$banner' where id=$userid";
            
            $conn->query($sql);
            
            
            $output = array('status' => 'true','message' => 'File upload successfully','image' => $banner);
            
        }
        else
        {
            $output = array('status' => 'false','message' => 'File upload failure');
        }
    }    
    else if($choice == "7")
    {
        // print_r($_REQUEST);
        $userid         = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
        $date           = date('Y-m-d H:i:s');
        $address       = ((!empty($_REQUEST['address'])) ? $_REQUEST['address'] : "");
        $first_name     = ((!empty($_REQUEST['first_name'])) ? $_REQUEST['first_name'] : "");
        $last_name      = ((!empty($_REQUEST['last_name'])) ? $_REQUEST['last_name'] : "");
        $birthday       = ((!empty($_REQUEST['birthday'])) ? $_REQUEST['birthday'] : "");
        $phone_number   = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
        
        $result = $conn->query("select * from patients where id=$userid");
        if($result->num_rows > 0)
        {
            
            $conn->query("update patients set address='$address',first_name='$first_name',last_name='$last_name',birthday='$birthday',updated_at='$date',phone_number='$phone_number' where id=$userid");
            $output = array('status' => 'true','message'=>'Successfully updated');
        }
        else {
            $output = array('status' => 'false','message'=>'Userid does not exist');
        }
        
    }
    echo json_encode($output);
    ?>
