<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

//insert patient medication
if($choice == "0")
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $quantity = ((!empty($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : "");
    $pain_score = ((!empty($_REQUEST['pain_score'])) ? $_REQUEST['pain_score'] : "");
    $pain_score_time = ((!empty($_REQUEST['pain_score_time'])) ? $_REQUEST['pain_score_time'] : "");
    $take_date = ((!empty($_REQUEST['take_date'])) ? $_REQUEST['take_date'] : "");
    $refill = ((!empty($_REQUEST['refill'])) ? $_REQUEST['refill'] : "");

    $sql = "insert into patient_medication(patient_id,medication_id,quantity,pain_score,pain_score_time,take_date,refill) values($userid,$drugid,$quantity,$pain_score,'$pain_score_time','$take_date',$refill)";
    $conn->query($sql);
    $output = array('status' => 'true','message' => "Successfully registered");
}
//get patient medications about patient id
else if($choice == "1")
{
    $id = ((!empty($_REQUEST['patient_id'])) ? $_REQUEST['patient_id'] : "");
    $sql = "select patients.*,medications.*,patient_medication.* from patient_medication left join patients on patient_medication.patient_id=patients.id left join medications on patient_medication.medication_id=medications.id where patient_id=$id";
    $result = $conn->query($sql);
    $info = array();
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
        $output = array('status' => 'true','data' => $info);
    }
    else{
        $output = array('status' => 'true','data' => $info);
    }
}
//get patient medications about medication id
else if($choice == "2")
{
    $id = ((!empty($_REQUEST['medication_id'])) ? $_REQUEST['medication_id'] : "");
    $sql = "select patients.*,medications.*,patient_medication.* from patient_medication left join patients on patient_medication.patient_id=patients.id left join medications on patient_medication.medication_id=medications.id where medication_id=$id";
    $result = $conn->query($sql);
    $info = array();
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
        $output = array('status' => 'true','data' => $info);
    }
    else{
        $output = array('status' => 'true','data' => $info);
    }
}
//get detail patient medication
else if($choice == "3")
{
    $id = ((!empty($_REQUEST['id'])) ? $_REQUEST['id'] : "");
    $sql = "select patients.*,medications.*,patient_medication.* from patient_medication left join patients on patient_medication.patient_id=patients.id left join medications on patient_medication.medication_id=medications.id where patient_id=$id";
    $result = $conn->query($sql);
    $info = array();
    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        
        $output = array('status' => 'true','data' => $row);
    }
    else{
        $output = array('status' => 'true','data' => $info);
    }
}
print(json_encode($output));
?>