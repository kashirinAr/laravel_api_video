<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

//get notifications for user 
if($choice == "0")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");   
    $data = array();    
    $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage,notification,type,createat from notifications left join patients on patients.id=notifications.senderid where notifications.receiverid=$userid";            
    $result = $conn->query($sql);
    
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    
    $output = array('status' => 'true','data' => $data);
}
print(json_encode($output));

?>
