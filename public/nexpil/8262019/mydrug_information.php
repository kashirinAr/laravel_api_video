<?php
require_once('./include/config.php');

$choice = $_POST['choice'];
if($choice == "0") //insert drug with userid
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $array = ((!empty($_REQUEST['datas'])) ? $_REQUEST['datas'] : "");
    $array = json_decode($array,true);

    for($i = 0; $i < count($array);$i++)
    {
        $directions = $array[$i]["direction"];
        $dose = $array[$i]["dose"];
        $quantity = "7";//$array[$i]["taketime"];
        $prescribe = $array[$i]["prescribe"];
        $taketime = $array[$i]["taketime"];
        $patientname = $array[$i]["patientname"];
        $pharmacy = $array[$i]["pharmacy"];
        $medicationname = $array[$i]["medicationname"];
        $strength = $array[$i]["strength"];
        $filed_date = $array[$i]["filed_date"];
        $warnings = $array[$i]["warnings"];
        $type = "tablet";
        $frequency = $array[$i]["frequency"];
        $lefttablet = $array[$i]["lefttablet"];
        $prescription = $array[$i]["prescription"];
        $createat = $array[$i]["createat"];
        $endat = $array[$i]["endat"];

        $image_name = '';
        $result = $conn->query("select * from mymedications where medicationname='$medicationname' and userid=$userid");
        if ($result->num_rows == 0)
        {
            $sql = "insert into mymedications(userid,directions,dose,image,quantity,prescribe,filed_date,warnings,taketime,patientname,pharmacy,medicationname,strength,frequency,lefttablet,prescription,createat, endat) 
            values($userid,'$directions','$dose','$image_name','$quantity','$prescribe','$filed_date','$warnings','$taketime','$patientname','$pharmacy','$medicationname','$strength','$frequency','$lefttablet',$prescription,'$createat', '$endat')";

            $conn->query($sql);
        }

    }

    $output = array('status' => 'true','message' => "Successfully saved");

}
else if($choice == "1") //get drugs with taketime
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $taketime = ((!empty($_REQUEST['taketime'])) ? $_REQUEST['taketime'] : "");
    $info = array();
    $result = $conn->query("select * from mymedications where userid=$userid and taketime='$taketime'");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $info);
}
else if($choice == "2") //update drug
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $directions = ((!empty($_REQUEST['directions'])) ? $_REQUEST['directions'] : "");
    $dose = ((!empty($_REQUEST['dose'])) ? $_REQUEST['dose'] : "");
    $quantity = ((!empty($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : "");
    $price = ((!empty($_REQUEST['price'])) ? $_REQUEST['price'] : "");
    $lowest_price = ((!empty($_REQUEST['lowest_price'])) ? $_REQUEST['lowest_price'] : "");
    $cheapest_price = ((!empty($_REQUEST['cheapest_price'])) ? $_REQUEST['cheapest_price'] : "");
    $comparison_price = ((!empty($_REQUEST['comparison_price'])) ? $_REQUEST['comparison_price'] : "");
    $expiration = ((!empty($_REQUEST['expiration'])) ? $_REQUEST['expiration'] : "");
    $manufacture = ((!empty($_REQUEST['manufacture'])) ? $_REQUEST['manufacture'] : "");
    $substituted = ((!empty($_REQUEST['substituted'])) ? $_REQUEST['substituted'] : "");
    $filed_date = ((!empty($_REQUEST['filed_date'])) ? $_REQUEST['filed_date'] : "");
    $warnings = ((!empty($_REQUEST['warnings'])) ? $_REQUEST['warnings'] : "");
    $type = ((!empty($_REQUEST['type'])) ? $_REQUEST['type'] : "tablet");

    if (!empty($_FILES['drug_image']['name'])) {
        //$tmp_name = $_FILES["drug_image"]["tmp_name"];
        $names = $_FILES["drug_image"]["name"];
        $image_name = date('dmYhis') . $names;
        //$image = 'BASE_URL' . $image_name;
        $uploadpath = "drugimage/" . $image_name;
        move_uploaded_file($_FILES["drug_image"]["tmp_name"], $uploadpath);

        $sql = "update medications set directions='$directions',dose='$dose',quantity=$quantity,price=$price,lowest_price=$lowest_price,cheapest_price=$cheapest_price,
            comparison_price=$comparison_price,expiration='$expiration',manufacture='$manufacture',substituted='$substituted',filed_date='$filed_date',warnings='$warnings',type='$type' 
            , image='$image_name' where id=$drugid";
    }
    else {
        $sql = "update medications set directions='$directions',dose='$dose',quantity=$quantity,price=$price,lowest_price=$lowest_price,cheapest_price=$cheapest_price,
            comparison_price=$comparison_price,expiration='$expiration',manufacture='$manufacture',substituted='$substituted',filed_date='$filed_date',warnings='$warnings',type='$type' 
            where id=$drugid";
    }
    $conn->query($sql);
    $output = array('status' => 'true','message' => 'Successfully updated.');
}
else if($choice == "3") //delete drug
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $conn->query("delete from mymedications where id=$drugid");
    $conn->query("delete from tbl_taken_drug where drug_id=$drugid");
    $output = array('status' => 'true','message' => 'Successfully deleted.');
}
else if($choice == "4") //sign up and register medication
{    
    $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
    
    $password = ((!empty($_REQUEST['password'])) ? $_REQUEST['password'] : "");
    $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
    
    $result = $conn->query("select * from patients where email='$email'");
    if($result->num_rows > 0)
    {
        $output = array('status' => 'false','message' => "Your email is in use");
    }
    else
    {
        $date = date('Y-m-d H:i:s');
        $usertype = ((!empty($_REQUEST['usertype'])) ? $_REQUEST['usertype'] : "patient");
        $language = ((!empty($_REQUEST['language'])) ? $_REQUEST['language'] : "");
        $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
        $street = ((!empty($_REQUEST['street'])) ? $_REQUEST['street'] : "");
        $city = ((!empty($_REQUEST['city'])) ? $_REQUEST['city'] : "");
        $state = ((!empty($_REQUEST['state'])) ? $_REQUEST['state'] : "");
        $zipcode = ((!empty($_REQUEST['zipcode'])) ? $_REQUEST['zipcode'] : "");
        $first_name = ((!empty($_REQUEST['first_name'])) ? $_REQUEST['first_name'] : "");
        $last_name = ((!empty($_REQUEST['last_name'])) ? $_REQUEST['last_name'] : "");
        $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
        $birthday = ((!empty($_REQUEST['birthday'])) ? $_REQUEST['birthday'] : "");
        $age = ((!empty($_REQUEST['age'])) ? $_REQUEST['age'] : "0");
        $diagnosis_date = ((!empty($_REQUEST['diagnosis_date'])) ? $_REQUEST['diagnosis_date'] : "");
        $application_location = ((!empty($_REQUEST['application_location'])) ? $_REQUEST['application_location'] : "");
        $location = ((!empty($_REQUEST['location'])) ? $_REQUEST['location'] : "");
        $sex = ((!empty($_REQUEST['sex'])) ? $_REQUEST['sex'] : "male");
        
        $array = ((!empty($_REQUEST['datas'])) ? $_REQUEST['datas'] : "");
        $array = json_decode($array,true);

        $hash = password_hash($password,PASSWORD_DEFAULT);

        $sql = "insert into patients(usertype,language,phone_number,email,password,street,city,state,zipcode,first_name,last_name,user_name,birthday,age,diagnosis_date,application_location,location,
        sex) values('$usertype','$language','$phone_number','$email','$hash','$street','$city','$state','$zipcode','$first_name','$last_name','$user_name','$birthday',
        $age,'$diagnosis_date','$application_location','$location','$sex')";

        $conn->query($sql);

        $last = $conn->insert_id;

        $result = $conn->query("select * from patients where id=$last");

        if ($result->num_rows > 0)
        {
            $row = $result->fetch_assoc();
            $row['password'] = "";               
            $userinfo = $row;

            for($i = 0; $i < count($array);$i++)
            {                
                $directions = $array[$i]["direction"];
                $dose = $array[$i]["dose"];
                $quantity = $array[$i]["quantity"];
                $prescribe = $array[$i]["prescribe"];
                $quantity = "7";//$array[$i]["taketime"];
                $patientname = $array[$i]["patientname"];
                $pharmacy = $array[$i]["pharmacy"];
                $medicationname = $array[$i]["medicationname"];
                $strength = $array[$i]["strength"];
                $filed_date = $array[$i]["filed_date"]; 
                $warnings = $array[$i]["warnings"];
                $type = "tablet";
                $frequency = $array[$i]["frequency"];
                $lefttablet = $array[$i]["lefttablet"];
                $prescription = $array[$i]["prescription"];
                $createat = $array[$i]["createat"];
                $endat = $array[$i]["endat"];

                $image_name = '';   
                $result = $conn->query("select * from mymedications where medicationname='$medicationname'");
                if ($result->num_rows == 0)
                {
                    $sql = "insert into mymedications(userid,directions,dose,image,quantity,prescribe,filed_date,warnings,taketime,patientname,pharmacy,medicationname,strength,frequency,lefttablet,prescription,createat, endat) 
                    values($last,'$directions','$dose','$image_name','$quantity','$prescribe','$filed_date','$warnings','$taketime','$patientname','$pharmacy','$medicationname','$strength','$frequency','$lefttablet',$prescription,'$createat', '$endat')";

                    $conn->query($sql);
                }
            }
            $output = array('status' => 'true','message' => 'You have signup successfully','userinfo' => $userinfo);
        } else {
            $output = array('status' => 'false','message' => 'Register failed');
        }      
    }
}
else if($choice == "5") //get drugs with taketime
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");    
    $info = array();
    $result = $conn->query("select * from mymedications where userid=$userid");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
    }

    $date = ((!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "");
    $addedDate = ((!empty($_REQUEST['addedDate'])) ? $_REQUEST['addedDate'] : "");    
    $takeInfo = array();
    $result = $conn->query("select * from tbl_take_drug where user_id=$userid AND (taken_time BETWEEN '$date' AND '$addedDate')");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $takeInfo[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $info, 'take_data' => $takeInfo);
}
else if($choice == "6") //update drug take time
{
    $medicationId = ((!empty($_REQUEST['medicationId'])) ? $_REQUEST['medicationId'] : "");
    $time = ((!empty($_REQUEST['time'])) ? $_REQUEST['time'] : "");
    $result = $conn->query("select * from mymedications where id=$medicationId");
    if ($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $createat = $row['createat'];
        $date = explode(" ",$createat)[0];
        $createat = $date." ". $time;
        $conn->query("update mymedications set createat='$createat' where id=$medicationId");
        $output = array('status' => 'true','message' => "Success");
    }
    else {
        $output = array('status' => 'false','message' => "Can not find drug information.");
    }
}
else if($choice == "7") //get drug with medicationId
{
    $medicationId = ((!empty($_REQUEST['medicationId'])) ? $_REQUEST['medicationId'] : "");
    $info = array();
    $result = $conn->query("select * from mymedications where id=$medicationId");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $info);
}
else if ($choice == "8") // get first drug usage date
{
    if (empty($_REQUEST['userid'])) {
        $output = array('status' => 'false','message' => "user id is empty");
    } else {
        $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");        
        $result = $conn->query("SELECT createat FROM `mymedications` where userid = $userid order by createat limit 1");
        $obj = $result->fetch_object();
        if (!empty($obj)) {
            $output = array('status' => 'true','createat' => $obj->createat);
        } else {
            $output = array('status' => 'false','message' => "no drug");
        }
    }
}
else if ($choice == "9")  // post take time
{
    $userId = ((!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : "");
    $drugId = ((!empty($_REQUEST['drug_id'])) ? $_REQUEST['drug_id'] : "");
    $date = ((!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "");
    $query = "INSERT INTO tbl_take_drug VALUES(null, $drugId, $userId, '$date')";    
    $result = $conn->query($query);
    $result = $conn->query("select quantity from mymedications where id=$drugId");
    $obj = $result->fetch_object();
    if (!empty($obj)) {
        $quantity = $obj->quantity - 1;
        $result = $conn->query("UPDATE mymedications SET quantity = $quantity where id=$drugId");
    } 
    $output = array('status' => 'true','message' => "Successfully saved");
}
else if ($choice == "10")  // untake drug
{
    $takenId = ((!empty($_REQUEST['taken_id'])) ? $_REQUEST['taken_id'] : "");
    $drugId = ((!empty($_REQUEST['drug_id'])) ? $_REQUEST['drug_id'] : "");
    $query = "DELETE FROM tbl_take_drug WHERE id = $takenId";    
    $result = $conn->query($query);    
    $result = $conn->query("select quantity from mymedications where id=$drugId");
    $obj = $result->fetch_object();
    if (!empty($obj)) {
        $quantity = $obj->quantity + 1;
        $result = $conn->query("UPDATE mymedications SET quantity = $quantity where id=$drugId");
    }
    $output = array('status' => 'true','message' => "Successfully untaken");
}
else if ($choice == "11") // update taken time
{
    $drug_id = ((!empty($_REQUEST['drug_id'])) ? $_REQUEST['drug_id'] : "");
    $date = ((!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "");
    $query = "UPDATE mymedications SET createat = '$date' WHERE id = $drug_id";    
    $result = $conn->query($query);    
    $output = array('status' => 'true','message' => "Successfully untaken");
}
print(json_encode($output));
?>
