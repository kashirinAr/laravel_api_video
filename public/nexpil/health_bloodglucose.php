<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

// Insert BloodGlucose Data
if ($choice == "0")
{
    $userid = $_REQUEST['userid'];
    $measurement = $_REQUEST['measurement'];
    $timing = $_REQUEST['timing'];
    $date = $_REQUEST['date'];

    // Check if there is already existing TimeIndex value
    $query = "SELECT * FROM health_bg_histories WHERE userid=$userid AND timing=$timing AND date='$date'";
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
    	if ($row = $result->fetch_assoc()) {
    		$id = $row['id'];
    		$query = "UPDATE health_bg_histories SET measurement=$measurement WHERE id=$id";
    		$result = $conn->query($query);
    	}
    } else {
    	$query = "INSERT INTO health_bg_histories(userid, measurement, timing, date) VALUES($userid, $measurement, $timing, '$date')";
    	$result = $conn->query($query);
    }

    $output = array('query' => $query);
}

// Fetch recent BloodGlucose measurement
if ($choice == "1")
{
	$userid = $_REQUEST['userid'];
	$date = $_REQUEST['date'];
	$query = "SELECT measurement FROM health_bg_histories WHERE userid=$userid AND date='$date' ORDER BY createdat DESC LIMIT 1";
	$result = $conn->query($query);
	$measurement = 0;
	if ($row = $result->fetch_assoc()) {
		$measurement = $row['measurement'];
	}
	$output = array('value' => $measurement);
}

// Get BloodGlucose history by date
if ($choice == "2")
{
	$userid = $_REQUEST['userid'];
	$date = $_REQUEST['date'];
	$query = "SELECT measurement, timing FROM health_bg_histories WHERE userid=$userid AND date='$date'";
	$result = $conn->query($query);
	$historyList = array();
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$historyList[] = $row;
		}
	}
	$output = array('history' => $historyList);
}

// Get BloodGlucose history by week or month
if ($choice == "3")
{
	$userid = $_REQUEST['userid'];
	$startdate = $_REQUEST['startdate'];
	$enddate = $_REQUEST['enddate'];
	$query = "SELECT date, measurement, timing FROM health_bg_histories WHERE userid=$userid AND date >= '$startdate' AND date <= '$enddate'";
	$result = $conn->query($query);
	$historyList = array();
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$historyList[] = $row;
		}
	}
	$output = array('history' => $historyList);
}

print(json_encode($output));
?>