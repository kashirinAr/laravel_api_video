<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

//insert provider
if($choice == "0")
{
    $provider_name = ((!empty($_REQUEST['provider_name'])) ? $_REQUEST['provider_name'] : "");
    $address = ((!empty($_REQUEST['address'])) ? $_REQUEST['address'] : "");
    $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : ""); 
    $specialty = ((!empty($_REQUEST['specialty'])) ? $_REQUEST['specialty'] : ""); 

    $sql = "insert into providers(provider_name,address,phone_number,specialty) values('$provider_name','$address','$phone_number','$specialty')";

    $conn->query($sql);

    $output = array('status' => 'true','message' => "Successfully registered");

}
//get provider
else if($choice == "1")
{
    $info = array();
    $result = $conn->query("select * from providers");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $info);
}
//update provider
else if($choice == "2")
{
    $providerid = ((!empty($_REQUEST['providerid'])) ? $_REQUEST['providerid'] : "");
    $provider_name = ((!empty($_REQUEST['provider_name'])) ? $_REQUEST['provider_name'] : "");
    $address = ((!empty($_REQUEST['address'])) ? $_REQUEST['address'] : "");
    $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
    $specialty = ((!empty($_REQUEST['specialty'])) ? $_REQUEST['specialty'] : "");

    
    $sql = "update providers set provider_name='$provider_name',address='$address',phone_number='$phone_number',specialty='$specialty' where id=$providerid";
    
    $conn->query($sql);
    $output = array('status' => 'true','message' => 'Successfully updated.');

}
//delete provider
else if($choice == "3")
{
    $providerid = ((!empty($_REQUEST['providerid'])) ? $_REQUEST['providerid'] : "");
    $conn->query("delete from providers where id=$providerid");
    $output = array('status' => 'true','message' => 'Successfully deleted.');
}
//get detail provider
else if($choice == "4")
{
    $providerid = ((!empty($_REQUEST['providerid'])) ? $_REQUEST['providerid'] : "");
    $result = $conn->query("select * from providers where id=$providerid");
    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $output = array('status' => 'true','data' => $row);
    }
    else{
        $output = array('status' => 'false','message' => "No data.");
    }
}
print(json_encode($output));

?>