<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

// create task
if ($choice == "0") {
    $select_type = ((!empty($_REQUEST['select_type'])) ? $_REQUEST['select_type'] : "");
    $task = ((!empty($_REQUEST['task'])) ? $_REQUEST['task'] : "");
    $start_date = ((!empty($_REQUEST['start_date'])) ? $_REQUEST['start_date'] : "");
    $end_date = ((!empty($_REQUEST['end_date'])) ? $_REQUEST['end_date'] : "");
    $frequency = ((!empty($_REQUEST['frequency'])) ? $_REQUEST['frequency'] : "");
    $location = ((!empty($_REQUEST['location'])) ? $_REQUEST['location'] : "");
    $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
    $website = ((!empty($_REQUEST['website'])) ? $_REQUEST['website'] : "");
    $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
    $fax_number = ((!empty($_REQUEST['fax_number'])) ? $_REQUEST['fax_number'] : "");
    $description = ((!empty($_REQUEST['description'])) ? $_REQUEST['description'] : "");
    $task_triggers = ((!empty($_REQUEST['task_triggers'])) ? $_REQUEST['task_triggers'] : "");
    $template = ((!empty($_REQUEST['templsate'])) ? $_REQUEST['template'] : "");

    $characters = '0123456789';
    $code = '';
    for ($i = 0; $i < 7; $i++) {
        $code .= $characters[mt_rand(0, strlen($characters) - 1)];
    }

    for($i = 0 ; $i < count($task)  ; $i++) {
        $descriptionPlainText = strip_tags($description[$i]);
        $sql = "INSERT INTO tasks(type_id, name, description, frequency, location, phone_no, fax_no, email, website, triggers, start_date, end_date,  owner_id, physician_id, code) VALUES('$select_type', '$task[$i]', '$descriptionPlainText','$frequency[$i]','$location[$i]','$phone_number[$i]','$fax_number[$i]','$email[$i]','$website[$i]','$task_triggers[$i]','$start_date[$i]','$end_date[$i]','111','1','$code')";
        $conn->query($sql);    
    }
    
    $output = array('status' => 'true','message' => "Successfully saved");
}
// get task by owner id
else if ($choice == "1") {
    $owner_id = ((!empty($_REQUEST['owner_id'])) ? $_REQUEST['owner_id'] : "");

    $sql = "SELECT * FROM tasks WHERE owner_id='$owner_id'";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $data);
}
// get task by task id
else if ($choice == "2") {
    $task_id = ((!empty($_REQUEST['task_id'])) ? $_REQUEST['task_id'] : "");

    $sql = "SELECT * FROM tasks WHERE id='$task_id'";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $data);
}
// get task by code
else if ($choice == "3") {
    $task_code = ((!empty($_REQUEST['task_code'])) ? $_REQUEST['task_code'] : "");

    $sql = "SELECT * FROM tasks WHERE code='$task_code'";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $data);
}
// delete task by owner id
else if($choice == "4") {
    $owner_id = ((!empty($_REQUEST['owner_id'])) ? $_REQUEST['owner_id'] : "");
    $sql = "DELETE FROM tasks WHERE owner_id='$owner_id'";
    $conn->query($sql);
    $output = array('status' => 'true','message' => "Successfully deleted");
}
// delete task by task id
else if($choice == "5") {
    $task_id = ((!empty($_REQUEST['task_id'])) ? $_REQUEST['task_id'] : "");
    $sql = "DELETE FROM tasks WHERE id='$task_id'";
    $conn->query($sql);
    $output = array('status' => 'true','message' => "Successfully deleted");
}
// delete task by code
else if($choice == "6") {
    $task_code = ((!empty($_REQUEST['task_code'])) ? $_REQUEST['task_code'] : "");
    $sql = "DELETE FROM tasks WHERE code='$task_code'";
    $conn->query($sql);
    $output = array('status' => 'true','message' => "Successfully deleted");
}
// update task by task id
else if ($choice == "7") {
    $id = ((!empty($_REQUEST['id'])) ? $_REQUEST['id'] : "");
    $select_type = ((!empty($_REQUEST['select_type'])) ? $_REQUEST['select_type'] : "");
    $task = ((!empty($_REQUEST['task'])) ? $_REQUEST['task'] : "");
    $start_date = ((!empty($_REQUEST['start_date'])) ? $_REQUEST['start_date'] : "");
    $end_date = ((!empty($_REQUEST['end_date'])) ? $_REQUEST['end_date'] : "");
    $frequency = ((!empty($_REQUEST['frequency'])) ? $_REQUEST['frequency'] : "");
    $location = ((!empty($_REQUEST['location'])) ? $_REQUEST['location'] : "");
    $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
    $website = ((!empty($_REQUEST['website'])) ? $_REQUEST['website'] : "");
    $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
    $fax_number = ((!empty($_REQUEST['fax_number'])) ? $_REQUEST['fax_number']: "");
    $description = ((!empty($_REQUEST['description'])) ? strip_tags($_REQUEST['description']) : "");
    $task_triggers = ((!empty($_REQUEST['task_triggers'])) ? $_REQUEST['task_triggers'] : "");
    $template = ((!empty($_REQUEST['templsate'])) ? $_REQUEST['template'] : "");

    $sql = "UPDATE tasks SET type_id='$select_type', name='task', description='$description', frequency='$frequency', location='$location', phone_no='$phone_number', fax_no='$fax_number', email='$email', website='$website', triggers='$task_triggers', start_date='$start_date', end_date='$end_date' WHERE id='$id'";
    $conn->query($sql);
    $output = array('status' => 'true','message' => "Successfully saved");
}
print(json_encode($output));
?>