<?php
require_once('./include/config.php');

$choice = $_POST['choice'];
if($choice == "0") //insert drug with userid
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $array = ((!empty($_REQUEST['datas'])) ? $_REQUEST['datas'] : "");
    $array = stripcslashes($array); 
    $array = json_decode($array,true);
    
    for($i = 0; $i < count($array);$i++)
    {

        $directions = $array[$i]["direction"];
        $dose = $array[$i]["dose"];
        $quantity = $array[$i]["quantity"];
        $prescribe = $array[$i]["prescribe"];
        $taketime = $array[$i]["taketime"];
        $patientname = $array[$i]["patientname"];
        $pharmacy = $array[$i]["pharmacy"];
        $medicationname = $array[$i]["medicationname"];
        $strength = $array[$i]["strength"];
        $filed_date = $array[$i]["filed_date"];
        $warnings = $array[$i]["warnings"];
        $type = "tablet";
        $frequency = $array[$i]["frequency"];
        $lefttablet = $array[$i]["lefttablet"];
        // $prescription = $array[$i]["prescription"];
        $createat = $array[$i]["createat"];
        $endat = $array[$i]["endat"];
        $amount = $array[$i]["amount"];        
        $asneeded = !empty($array[$i]["asneeded"]) ? $array[$i]["asneeded"] : 0;

        $image_name = '';
        $query = "select * from mymedications where medicationname='$medicationname' and userid=$userid AND taketime='$taketime'";
        $result = $conn->query($query);

        if ($result->num_rows == 0)
        {
            $sql = "insert into mymedications(userid,directions,dose,image,quantity,prescribe,filed_date,warnings,taketime,patientname,pharmacy,medicationname,strength,frequency,lefttablet,asneeded,createat, endat, amount) 
            values($userid,'$directions','$dose','$image_name','$quantity','$prescribe','$filed_date','$warnings','$taketime','$patientname','$pharmacy','$medicationname','$strength','$frequency','$lefttablet',$asneeded,'$createat', '$endat', '$amount')";

            $conn->query($sql);
        }

    }

    $output = array('status' => 'true','message' => "Successfully saved", "data" => $array[0]);

}
else if($choice == "1") //get drugs with taketime
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $taketime = ((!empty($_REQUEST['taketime'])) ? $_REQUEST['taketime'] : "");
    $info = array();
    $result = $conn->query("select * from mymedications where userid=$userid and taketime='$taketime'");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $info);
}
else if($choice == "2") //update drug
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $directions = ((!empty($_REQUEST['directions'])) ? $_REQUEST['directions'] : "");
    $dose = ((!empty($_REQUEST['dose'])) ? $_REQUEST['dose'] : "");
    $quantity = ((!empty($_REQUEST['quantity'])) ? $_REQUEST['quantity'] : "");
    $price = ((!empty($_REQUEST['price'])) ? $_REQUEST['price'] : "");
    $lowest_price = ((!empty($_REQUEST['lowest_price'])) ? $_REQUEST['lowest_price'] : "");
    $cheapest_price = ((!empty($_REQUEST['cheapest_price'])) ? $_REQUEST['cheapest_price'] : "");
    $comparison_price = ((!empty($_REQUEST['comparison_price'])) ? $_REQUEST['comparison_price'] : "");
    $expiration = ((!empty($_REQUEST['expiration'])) ? $_REQUEST['expiration'] : "");
    $manufacture = ((!empty($_REQUEST['manufacture'])) ? $_REQUEST['manufacture'] : "");
    $substituted = ((!empty($_REQUEST['substituted'])) ? $_REQUEST['substituted'] : "");
    $filed_date = ((!empty($_REQUEST['filed_date'])) ? $_REQUEST['filed_date'] : "");
    $warnings = ((!empty($_REQUEST['warnings'])) ? $_REQUEST['warnings'] : "");
    $type = ((!empty($_REQUEST['type'])) ? $_REQUEST['type'] : "tablet");

    if (!empty($_FILES['drug_image']['name'])) {
        //$tmp_name = $_FILES["drug_image"]["tmp_name"];
        $names = $_FILES["drug_image"]["name"];
        $image_name = date('dmYhis') . $names;
        //$image = 'BASE_URL' . $image_name;
        $uploadpath = "drugimage/" . $image_name;
        move_uploaded_file($_FILES["drug_image"]["tmp_name"], $uploadpath);

        $sql = "update medications set directions='$directions',dose='$dose',quantity=$quantity,price=$price,lowest_price=$lowest_price,cheapest_price=$cheapest_price,
            comparison_price=$comparison_price,expiration='$expiration',manufacture='$manufacture',substituted='$substituted',filed_date='$filed_date',warnings='$warnings',type='$type' 
            , image='$image_name' where id=$drugid";
    }
    else {
        $sql = "update medications set directions='$directions',dose='$dose',quantity=$quantity,price=$price,lowest_price=$lowest_price,cheapest_price=$cheapest_price,
            comparison_price=$comparison_price,expiration='$expiration',manufacture='$manufacture',substituted='$substituted',filed_date='$filed_date',warnings='$warnings',type='$type' 
            where id=$drugid";
    }
    $conn->query($sql);
    $output = array('status' => 'true','message' => 'Successfully updated.');
}
else if($choice == "3") //delete drug
{
    $drugid = ((!empty($_REQUEST['drugid'])) ? $_REQUEST['drugid'] : "");
    $conn->query("delete from mymedications where id=$drugid");
    $conn->query("delete from tbl_taken_drug where drug_id=$drugid");
    $output = array('status' => 'true','message' => 'Successfully deleted.');
}
else if($choice == "4") //sign up and register medication
{    
    $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
    
    $password = ((!empty($_REQUEST['password'])) ? $_REQUEST['password'] : "");
    $email = ((!empty($_REQUEST['email'])) ? $_REQUEST['email'] : "");
    
    $result = $conn->query("select * from patients where email='$email'");
    if($result->num_rows > 0)
    {
        $output = array('status' => 'false','message' => "Your email is in use");
    }
    else
    {
        $date = date('Y-m-d H:i:s');
        $usertype = ((!empty($_REQUEST['usertype'])) ? $_REQUEST['usertype'] : "patient");
        $language = ((!empty($_REQUEST['language'])) ? $_REQUEST['language'] : "");
        $phone_number = ((!empty($_REQUEST['phone_number'])) ? $_REQUEST['phone_number'] : "");
        $street = ((!empty($_REQUEST['street'])) ? $_REQUEST['street'] : "");
        $city = ((!empty($_REQUEST['city'])) ? $_REQUEST['city'] : "");
        $state = ((!empty($_REQUEST['state'])) ? $_REQUEST['state'] : "");
        $zipcode = ((!empty($_REQUEST['zipcode'])) ? $_REQUEST['zipcode'] : "");
        $first_name = ((!empty($_REQUEST['first_name'])) ? $_REQUEST['first_name'] : "");
        $last_name = ((!empty($_REQUEST['last_name'])) ? $_REQUEST['last_name'] : "");
        $user_name = ((!empty($_REQUEST['user_name'])) ? $_REQUEST['user_name'] : "");
        $birthday = ((!empty($_REQUEST['birthday'])) ? $_REQUEST['birthday'] : "");
        $age = ((!empty($_REQUEST['age'])) ? $_REQUEST['age'] : "0");
        $diagnosis_date = ((!empty($_REQUEST['diagnosis_date'])) ? $_REQUEST['diagnosis_date'] : "");
        $application_location = ((!empty($_REQUEST['application_location'])) ? $_REQUEST['application_location'] : "");
        $location = ((!empty($_REQUEST['location'])) ? $_REQUEST['location'] : "");
        $sex = ((!empty($_REQUEST['sex'])) ? $_REQUEST['sex'] : "male");
        
        $array = ((!empty($_REQUEST['datas'])) ? $_REQUEST['datas'] : "");
        $array = stripcslashes($array); 
        $array = json_decode($array,true);

        
        $hash = password_hash($password,PASSWORD_DEFAULT);

        $sql = "insert into patients(usertype,language,phone_number,email,password,street,city,state,zipcode,first_name,last_name,user_name,birthday,age,diagnosis_date,application_location,location, sex) values('$usertype','$language','$phone_number','$email','$hash','$street','$city','$state','$zipcode','$first_name','$last_name','$user_name','$birthday',$age,'$diagnosis_date','$application_location','$location','$sex')";

        $conn->query($sql);

        $last = $conn->insert_id;

        
        $result = $conn->query("select * from patients where id=$last");

        if ($result->num_rows > 0)
        {
            $row = $result->fetch_assoc();
            $row['password'] = "";               
            $userinfo = $row;


            for($i = 0; $i < count($array);$i++)
            {

                $directions = $array[$i]["direction"];
                $dose = $array[$i]["dose"];
                $quantity = $array[$i]["quantity"];
                $prescribe = $array[$i]["prescribe"];
                $taketime = $array[$i]["taketime"];
                $patientname = $array[$i]["patientname"];
                $pharmacy = $array[$i]["pharmacy"];
                $medicationname = $array[$i]["medicationname"];
                $strength = $array[$i]["strength"];
                $filed_date = $array[$i]["filed_date"];
                $warnings = $array[$i]["warnings"];
                $type = "tablet";
                $frequency = $array[$i]["frequency"];
                $lefttablet = $array[$i]["lefttablet"];
                // $prescription = $array[$i]["prescription"];
                $createat = $array[$i]["createat"];
                $endat = $array[$i]["endat"];
                $amount = $array[$i]["amount"];        
                $asneeded = !empty($array[$i]["asneeded"]) ? $array[$i]["asneeded"] : 0;

                $image_name = '';
                $query = "select * from mymedications where medicationname='$medicationname' and userid=$last AND taketime='$taketime'";
                $result = $conn->query($query);

                if ($result->num_rows == 0)
                {
                    $sql = "insert into mymedications(userid,directions,dose,image,quantity,prescribe,filed_date,warnings,taketime,patientname,pharmacy,medicationname,strength,frequency,lefttablet,asneeded,createat, endat, amount) 
                    values($last,'$directions','$dose','$image_name','$quantity','$prescribe','$filed_date','$warnings','$taketime','$patientname','$pharmacy','$medicationname','$strength','$frequency','$lefttablet',$asneeded,'$createat', '$endat', '$amount')";

                    $conn->query($sql);
                }

            }
            $output = array('status' => 'true','message' => 'You have signup successfully','userinfo' => $userinfo);
        } else {
            $output = array('status' => 'false','message' => 'Register failed', 'sql' => $sql);
        }      
    }
}
else if($choice == "5") //get drugs with taketime
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");    
    $date = ((!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "");    
    $time = ((!empty($_REQUEST['time'])) ? $_REQUEST['time'] : "");

    // Get Normal Drug info
    $start_time = 0;
    $end_time = 12;
    switch ($time) {
        case "0": // morning
            $start_time = 0;
            $end_time = 12;
        break;
        case "1": //midday
            $start_time = 12;
            $end_time = 17;
            break;
        case "2": //evening
            $start_time = 17;
            $end_time = 20;
            break;
        case "3": //night
            $start_time = 20;
            $end_time = 24;
            break;
    }
    
    $query = "select * from (select taketime as timing, id,CONVERT(SUBSTRING(taketime, 1, 2), INTEGER) as taketime from mymedications where userid=$userid AND '$date' BETWEEN date_sub(createat, INTERVAL 1 day) AND date_add(createat, INTERVAL 30 day) AND asneeded=0  AND quantity > 0 GROUP BY taketime) as result where taketime >= $start_time AND taketime < $end_time";
    $result = $conn->query($query);
    
    $timings = array();
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $timings[] = $row;
        }
    }

    $drug_info = array();
    for($i = 0; $i < count($timings); $i++) {
        $drug_id = $timings[$i]["id"];
        $taketime = (int)$timings[$i]["taketime"];
        $timing = $timings[$i]["timing"];

        // Get drug take history
        $take_drugs = array();
        $query = "select * from (select A.taken_time, B.medicationname, B.taketime as taketimes, B.quantity, B.id, B.amount, A.id historyId, CONVERT(SUBSTRING(B.taketime, 1, 2), INTEGER) as taketime from tbl_take_drug A LEFT JOIN mymedications B ON A.drug_id=B.id where user_id=$userid AND DATE(taken_time)='$date') as result where taketime=$taketime";
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $take_drugs[] = $row;
            }
        }

        // Get untaken drugs
        $untake_drugs = array();
        $query = "select * from (select id,medicationname,taketime as taketimes,quantity,amount,createat,CONVERT(SUBSTRING(taketime, 1, 2), INTEGER) as taketime from mymedications where userid=$userid AND '$date' BETWEEN date_sub(createat, INTERVAL 1 day) AND date_add(createat, INTERVAL 30 day) AND asneeded=0) as result where taketime=$taketime";
        $result = $conn->query($query);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $is_taken = false;
                for ($j = 0; $j < count($take_drugs); $j++) {
                    if ($take_drugs[$j]["id"] == $row["id"]) {
                        $is_taken = true;
                    }
                }
                if (!$is_taken) {
                    $untake_drugs[] = $row;
                }
            }
        }

        // Calculate timing string
        $hour = (int)substr($timing, 0, 2);
        if ($time > 0) {
            $hour -= 12;
        }
        $timing_str = $hour.":".substr($timing, 2, 2);
        $timing_str .= (int)$time == 0 ? "am" : "pm";
        
        
        $drug_info[] = array('timing' => $timing_str, 'take_drugs' => $take_drugs, 'untake_drugs' => $untake_drugs, 'taketime' => $taketime);
    }

    $new_drugInfo = array();
    // Group 2 hours drug
    for ($i = 0; $i < count($drug_info); $i++) {
        $drug1 = $drug_info[$i];
        $group_status = 0;
        $merge_drug = array();
        for ($j = 0; $j < count($drug_info); $j++) {
            $drug2 = $drug_info[$j];
            
            if ($drug1["taketime"] - $drug2["taketime"] == -1) {
                $group_status = 1;
            }
            if ($drug1["taketime"] - $drug2["taketime"] == 1) {
                $group_status = 2;
                $merge_drug = $drug2;
            }
        }
        
        if ($group_status == 0) {
            $new_drugInfo[] = $drug1;
        } else if ($group_status == 2) {
            $new_drugInfo[] = array(
                'timing' => $drug1["timing"], 
                'take_drugs' => array_merge($drug1["take_drugs"], $merge_drug["take_drugs"]), 
                'untake_drugs' => array_merge($drug1["untake_drugs"], $merge_drug["untake_drugs"]),
            );
        }
    }

    // Get Asneeded Medication
    /**
     * select
taketime,
  SUBSTRING_INDEX(SUBSTRING_INDEX(mymedications.taketime, ',', numbers.n), ',', -1) as name
from
  (select 1 n union all
   select 2 union all 
   select 3 union all
   select 4 union all 
   select 5 union all
   select 6) numbers INNER JOIN mymedications
  on CHAR_LENGTH(mymedications.taketime)
     -CHAR_LENGTH(REPLACE(mymedications.taketime, ',', ''))>=numbers.n-1
where asneeded=1
order by
  id, n
     */
    $asneeded = array();
    $query = "select id, medicationname, amount, frequency, directions, strength, createat from mymedications where userid=$userid AND '$date' BETWEEN date_sub(createat, INTERVAL 1 day) AND date_add(createat, INTERVAL 30 day) AND asneeded=1";
    $result = $conn->query($query);
    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            $history = array();
            $drug_id = $row["id"];
            $query = "select A.*, B.amount, TIMEDIFF(NOW(), A.taken_time) timediff from tbl_take_drug A left join mymedications B ON A.drug_id=B.id where A.drug_id = $drug_id AND DATE(A.taken_time)='$date'";
            $result1 = $conn->query($query);
            if ($result1->num_rows > 0) {
                while ($row1 = $result1->fetch_assoc()) {
                    $history[] = $row1;
                }
            }
            $asneeded[] = array(
                "id" => $row["id"], "medicationname" => $row["medicationname"], "amount" => $row["amount"], "frequency" => $row["frequency"], "directions" => $row["directions"], "strength" => $row["strength"], "createat" => $row["createat"], "history" => $history
            );
        }
    }

    $output = array('drug_info' => $new_drugInfo, 'asneeded' => $asneeded);

    
    // $info = array();
    // $query = "select A.* from mymedications A where userid=$userid AND '$date' BETWEEN date_sub(createat, INTERVAL 1 day) AND date_add(createat, INTERVAL 30 day)";
    // $result = $conn->query($query);
    // if($result->num_rows > 0)
    // {
    //     while($row = $result->fetch_assoc())
    //     {
    //         $info[] = $row;
    //     }
    // }

    // $takeInfo = array();
    // for($i = 0; $i < count($info);$i++) {        
    //     $drugId = $info[$i]["id"];
    //     $result = $conn->query("select * from tbl_take_drug where user_id=$userid AND drug_id = $drugId AND DATE(taken_time)='$date'");        
    //     if($row = $result->fetch_assoc())
    //     {
    //         $takeInfo[] = $row;
    //     }
    // }

    // $query = "select IFNULL(GROUP_CONCAT(B.take_time), '') as history from mymedications A LEFT JOIN drug_histories B ON A.id=B.drug_id where userid=$userid  AND DATE(take_time)='$date' GROUP BY A.id";
    // $result = $conn->query($query);
    // $history = $result->fetch_assoc();
    
    // $output = array('status' => 'true','data' => $info, 'take_data' => $takeInfo, 'history' => $history, 'query' => $query);
}
else if($choice == "6") //update drug take time
{
    $medicationId = ((!empty($_REQUEST['medicationId'])) ? $_REQUEST['medicationId'] : "");
    $time = ((!empty($_REQUEST['time'])) ? $_REQUEST['time'] : "");
    $result = $conn->query("select * from mymedications where id=$medicationId");
    if ($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $createat = $row['createat'];
        $date = explode(" ",$createat)[0];
        $createat = $date." ". $time;
        $conn->query("update mymedications set createat='$createat' where id=$medicationId");
        $output = array('status' => 'true','message' => "Success");
    }
    else {
        $output = array('status' => 'false','message' => "Can not find drug information.");
    }
}
else if($choice == "7") //get drug with medicationId
{
    $medicationId = ((!empty($_REQUEST['medicationId'])) ? $_REQUEST['medicationId'] : "");
    $date = ((!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "");

    $info = array();
    $query = "SELECT A.*, B.taken_time, B.id takenId FROM `mymedications` A LEFT JOIN tbl_take_drug B ON A.id=B.drug_id where A.id=$medicationId";
    $result = $conn->query($query);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info = $row;
        }
    }
    $output = array('status' => 'true','data' => $info);
}
else if ($choice == "8") // get first drug usage date
{
    if (empty($_REQUEST['userid'])) {
        $output = array('status' => 'false','message' => "user id is empty");
    } else {
        $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");        
        $result = $conn->query("SELECT createat FROM `mymedications` where userid = $userid order by createat limit 1");
        $obj = $result->fetch_object();
        if (!empty($obj)) {
            $output = array('status' => 'true','createat' => $obj->createat);
        } else {
            $output = array('status' => 'false','message' => "no drug");
        }
    }
}
else if ($choice == "9")  // post take time
{
    $userId = ((!empty($_REQUEST['user_id'])) ? $_REQUEST['user_id'] : "");
    $drugId = ((!empty($_REQUEST['drug_id'])) ? $_REQUEST['drug_id'] : "");
    $date = ((!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "");
    $query = "INSERT INTO tbl_take_drug VALUES(null, $drugId, $userId, '$date')";    
    $result = $conn->query($query);
    $result = $conn->query("select quantity from mymedications where id=$drugId");
    $obj = $result->fetch_object();
    if (!empty($obj)) {
        $quantity = $obj->quantity - 1;
        $result = $conn->query("UPDATE mymedications SET quantity = $quantity where id=$drugId");
    } 
    $output = array('status' => 'true','message' => "Successfully saved", 'query' => $query);
}
else if ($choice == "10")  // untake drug
{
    $takenId = ((!empty($_REQUEST['taken_id'])) ? $_REQUEST['taken_id'] : "");
    $drugId = ((!empty($_REQUEST['drug_id'])) ? $_REQUEST['drug_id'] : "");
    // Get back quantity
    $query = "SELECT B.quantity FROM tbl_take_drug A LEFT JOIN mymedications B ON A.drug_id=B.id WHERE A.id=$takenId";
    $result = $conn->query($query);
    $obj = $result->fetch_object();
    if (!empty($obj)) {
        $quantity = $obj->quantity + 1;
        $result = $conn->query("UPDATE mymedications SET quantity = $quantity where id=$drugId");
    }
    
    // Remove taken history
    $query = "DELETE FROM tbl_take_drug WHERE id = $takenId";    
    $conn->query($query);
    
    $output = array('status' => 'true','message' => "Successfully untaken", 'query' => $query);
}
else if ($choice == "11") // update taken time
{
    $drug_id = ((!empty($_REQUEST['drug_id'])) ? $_REQUEST['drug_id'] : "");
    $date = ((!empty($_REQUEST['date'])) ? $_REQUEST['date'] : "");
    $query = "UPDATE mymedications SET createat = '$date' WHERE id = $drug_id";    
    $result = $conn->query($query);    
    $output = array('status' => 'true','message' => "Successfully untaken");
}
else if ($choice == "12")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");

    $firstMedicaionDate = "";
    $result = $conn->query("SELECT DATE(createat) as createat FROM `mymedications` WHERE userid = $userid ORDER BY createat ASC LIMIT 1");
    if ($row = $result->fetch_assoc())
    {
        $firstMedicaionDate = $row["createat"];
    }
    $medication_createList = array();
    $result = $conn->query("SELECT * FROM (SELECT Date(createat) as createat FROM `mymedications` WHERE userid = $userid GROUP BY medicationname) as result GROUP BY createat");
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $medication_createList[] = $row;
        }
    }
    $output = array('status' => 'true', 'first_date' => $firstMedicaionDate, 'medication_createList' => $medication_createList);
}
else if ($choice == "13")
{
    $id = ((!empty($_REQUEST['id'])) ? $_REQUEST['id'] : "");
    
    $query = "SELECT HOUR(taken_time) as hour, MINUTE(taken_time) as min FROM tbl_take_drug WHERE id=$id";
    $result = $conn->query($query);
    
    $obj = $result->fetch_object();
    if (!empty($obj)) {
        $output = array('status' => 'true','hour' => $obj->hour, 'min' => $obj->min);
    } else {
        $output = array('status' => 'false','message' => "no history or invalid id");
    }
}
else if ($choice == "14") // Update Taken Time
{
    $id = ((!empty($_REQUEST['id'])) ? $_REQUEST['id'] : "");
    $hour = ((!empty($_REQUEST['hour'])) ? $_REQUEST['hour'] : "");
    $min = ((!empty($_REQUEST['min'])) ? $_REQUEST['min'] : "");
    
    $query = "UPDATE tbl_take_drug SET taken_time = DATE_ADD(taken_time, INTERVAL($hour - HOUR(taken_time)) HOUR), taken_time = DATE_ADD(taken_time, INTERVAL($min - MINUTE(taken_time)) MINUTE) WHERE id=$id";
    $result = $conn->query($query);
    $output = array("result" =>"taken time updated", "query"=>$query);
}
else if ($choice == "15") // Get Mymedications
{
    $userid = $_REQUEST['userid'];
    $query = "SELECT medicationname, id FROM `mymedications` where userid = $userid GROUP BY medicationname";
    $result = $conn->query($query);

    $medicationList = array();
    if ($result->num_rows > 0)
    {
        while ($row = $result->fetch_assoc())
        {
            $medicationList[] = $row;
        }
    }
    $output = array('status' => 'true', 'data' => $medicationList);
}
else if ($choice == "16") // Remove Medication by name
{
    $userId = $_REQUEST['userid'];
    $drugName = $_REQUEST['drugname'];
    // Get drugId by drugName for remove Taken Histories
    $query = "SELECT id FROM mymedications WHERE medicationname='$drugName' AND userid=$userId";
    $result = $conn->query($query);

    $drugIds = array();
    if ($result->num_rows > 0)
    {
        while ($row = $result->fetch_assoc())
        {
            $drugIds[] = $row['id'];
        }
    }

    // Remove Taken Histories
    $query = "DELETE FROM tbl_take_drug WHERE drug_id IN (";
    for ($i = 0; $i < count($drugIds); $i++)
    {
        $query .= $drugIds[$i];
        if ($i < count($drugIds) - 1)
        {
            $query .= ',';
        }
    }
    $query .= ")";
    $conn->query($query);
    
    // Remove drugInfo by drugName and userId
    $query = "DELETE FROM mymedications WHERE medicationname='$drugName' AND userid=$userId";
    $conn->query($query);

    $output = array('status' => 'true');
}
else if($choice == "17") //get drug with medicationId
{
    $medicationId = ((!empty($_REQUEST['medicationId'])) ? $_REQUEST['medicationId'] : "-1");
    $pillsAmount = ((!empty($_REQUEST['pillsAmount'])) ? $_REQUEST['pillsAmount'] : 0);
    
    $info = array();
    $query = "SELECT * FROM mymedications WHERE id='".$medicationId."'";
    $result = $conn->query($query);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $info = $row;
            $newQuantity = $info['quantity'] + $pillsAmount;
            $updateQuery = "UPDATE mymedications SET quantity = '".$newQuantity."' WHERE id='".$medicationId."'";
            if($conn->query($updateQuery) == true){
                $output = array('status' => 'true');
            }
        }
    } else {
        $output = array('status' => 'false');
    }
}
print(json_encode($output));
?>
