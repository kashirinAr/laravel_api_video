<?php
// Get the PHP helper library from twilio.com/docs/php/install
require __DIR__ . '/twilio-php-master/src/Twilio/autoload.php';
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;

// Substitute your Twilio AccountSid and ApiKey details
$accountSid = 'AC3f8dec2b88fa8aa612f23c0d281e3ab6';
$apiKeySid = 'SK103b3168f1a28c1c9cb4f312dc1f3bea';
$apiKeySecret = 'm3q8T4HhKL9aEtn5pd9GZWbKUhdiLMwW';

// A unique identifier for this user
$identity = $_GET['identity'];
// The specific Room we'll allow the user to access
$roomName =  $_GET['roomName'];

// Create an Access Token
$token = new AccessToken(
    $accountSid,
    $apiKeySid,
    $apiKeySecret,
    3600,
    $identity
);

// Create Video grant
$videoGrant = new VideoGrant();
$videoGrant->setRoom($roomName);

// Add grant to token
$token->addGrant($videoGrant);
// render token to string
$acc_token = ["access_token" => $token->toJWT()];
echo json_encode($acc_token);
?>