<?php
require_once('./include/config.php');

$choice = $_POST['choice'];

//get members to follow me
if($choice == "0")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");   
    $data = array();
    $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage,posts.id,posts.type,filename,mediaurl,createat,content,moodstate, health_value, health_type from posts left join patients on patients.id=posts.userid where posts.userid=$userid order by posts.createat desc";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $id = $row['id'];
            $sql = "select count(*) as likecounts from post_like where postid=$id";
            $result1 = $conn->query($sql);
            $row1 = $result1->fetch_assoc();
            $row['likecounts'] = $row1['likecounts'];
            $sql = "select count(*) as commentcounts from post_comment where postid=$id";
            $result1 = $conn->query($sql);
            $row1 = $result1->fetch_assoc();
            $row['commentcounts'] = $row1['commentcounts'];
            //get comment user images about each post comment users
            $sql = "select userimage, first_name, last_name, content, createat from post_comment left join patients on post_comment.userid=patients.id where postid=$id";
            $result1 = $conn->query($sql);
            $data1 = array();

            if($result1->num_rows > 0)
            {
                while($row1 = $result1->fetch_assoc())
                {
                    $data1[] = $row1;
                }
            }
            $row['comments'] = $data1;
            $data[] = $row;
        }
    }
    $sql = "select patients.id as userid,patients.first_name as firstname,patients.last_name as lastname,patients.userimage,phone_number as phonenumber from patients  where id=$userid";
    $result = $conn->query($sql);
    $user = array();
    if($result->num_rows > 0)
    {
        $user = $result->fetch_assoc();
    }
    $output = array('status' => 'true','data' => $data,'user' => $user);
}
//post community(photo,video)
else if($choice == "1")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : ""); 
    $type =  $_REQUEST['type'];
    $healthType = ((!empty($_REQUEST['health_type'])) ? $_REQUEST['health_type'] : "");
    $healthValue = ((!empty($_REQUEST['health_value'])) ? $_REQUEST['health_value'] : "");
    $content = ((!empty($_REQUEST['content'])) ? $_REQUEST['content'] : "");
    $filename = ((!empty($_REQUEST['filename'])) ? $_REQUEST['filename'] : "");
    $mediaurl = ((!empty($_REQUEST['mediaurl'])) ? $_REQUEST['mediaurl'] : "");
    $createat = ((!empty($_REQUEST['createat'])) ? $_REQUEST['createat'] : "");    
    $moodvalue = ((!empty($_REQUEST['moodvalue'])) ? $_REQUEST['moodvalue'] : "");
    $name = "name";
    if (!empty($_FILES[$name]['name'])) {
            
        $names = $_FILES[$name]["name"];
        $names = preg_replace('/\s+/', '', $names);
        $banner = date('dmYhis') . $names;
        if($type == "1"){
            $uploadpath = "./upload/post/photo/" . $banner;
            $content = "Shared photo";
        }
        else{
            $uploadpath = "./upload/post/video/" . $banner;
            $content = "Shared video";
        }
        $file = $_FILES[$name];        
        move_uploaded_file($_FILES[$name]["tmp_name"], $uploadpath);        
        $sql = "insert into posts(userid,type,content,filename,mediaurl,createat) values($userid,'$type','$content','$banner','$mediaurl','$createat')";
        $conn->query($sql);   
        $sql = "select userId from communityusers where friendId=$userid";
        $result = $conn->query($sql);
        
        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $friendId = $row['userId'];                
                $sql = "INSERT INTO notifications VALUES(null, $userid, $friendId, '$content', 0, 0, '$createat')";                
                $conn->query($sql);
            }
        }     
        $output = array('status' => 'true','message' => 'File upload success','size'=>$file['size']);      
    }
    else
    {            
        $sql = "insert into posts(userid,type,content,filename,mediaurl,moodstate,createat, health_type, health_value) values($userid,'$type','$content','','$mediaurl','$moodvalue','$createat', '$healthType', '$healthValue')";            
        $conn->query($sql);
        $sql = "select userId from communityusers where friendId=$userid";
        $result = $conn->query($sql);
        if($result->num_rows > 0)
        {
            while($row = $result->fetch_assoc())
            {
                $friendId = $row['userId'];                
                $sql = "INSERT INTO notifications VALUES(null, $userid, $friendId, '$content', 0, 0, '$createat')";                
                $conn->query($sql);
            }
        }
        
        $output = array('status' => 'true');
    }
}
//add comment
else if($choice == "2")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $postid = ((!empty($_REQUEST['postid'])) ? $_REQUEST['postid'] : "");
    $content = ((!empty($_REQUEST['content'])) ? $_REQUEST['content'] : "");
    $createat = ((!empty($_REQUEST['createat'])) ? $_REQUEST['createat'] : "");
    $sql = "insert into post_comment(postid,userid,content,createat) values($postid,$userid,'$content','$createat')";
    $conn->query($sql);  
    $output = array('status' => 'true','message' => 'success');
}
//add like
else if($choice == "3")
{
    $userid = ((!empty($_REQUEST['userid'])) ? $_REQUEST['userid'] : "");
    $postid = ((!empty($_REQUEST['postid'])) ? $_REQUEST['postid'] : "");    
    $createat = ((!empty($_REQUEST['createat'])) ? $_REQUEST['createat'] : "");
    $sql = "select * from post_like where postid=$postid and userid=$userid";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        $output = array('status' => 'false','message' => 'You have liked already.');
    }
    else {
        $sql = "insert into post_like(postid,userid,createat) values($postid,$userid,'$createat')";
        $conn->query($sql);  
        $output = array('status' => 'true','message' => 'success');
    }
}
//get users photos for comments
else if($choice == "4")
{
    $postid = ((!empty($_REQUEST['postid'])) ? $_REQUEST['postid'] : "");
    $sql = "select userimage from post_comment left join patients on post_comment.userid=patients.id where postid=$postid";
    $result = $conn->query($sql);
    $data = array();
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    $output = array('status' => 'true','data' => $data);
}

//get post user and comment users about postid
else if($choice == "5")
{
    $postid = ((!empty($_REQUEST['postid'])) ? $_REQUEST['postid'] : "");
    $sql = "select userimage,first_name as firstname,last_name as lastname,content from post_comment left join patients on post_comment.userid=patients.id where postid=$postid";
    $result = $conn->query($sql);
    $data = array();
    if($result->num_rows > 0)
    {
        while($row = $result->fetch_assoc())
        {
            $data[] = $row;
        }
    }
    $data1 = array();
    $sql = "select userimage,first_name as firstname,last_name as lastname,posts.* from posts left join patients on posts.userid=patients.id where posts.id=$postid";
    $result = $conn->query($sql);
    if($result->num_rows > 0)
    {
        $row = $result->fetch_assoc();
        $data1 = $row;
    }
    $output = array('status' => 'true','comments' => $data,'post' => $data1);
}
print(json_encode($output));

?>