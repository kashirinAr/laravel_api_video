<?php 
    // snooze for notification
    $snooze = (int)$_REQUEST['snooze'];
    $userid = $_REQUEST['userid'];

    sleep(10);
    // include "cronjob_handler.php";
    $sql = "SELECT A.id, A.deviceToken FROM patients A WHERE A.id=$userid";
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            sendAPNS($row["deviceToken"]);
        }
    }    

    function sendAPNS($deviceToken) {
        /* We are using the sandbox version of the APNS for development. For production
        environments, change this to ssl://gateway.push.apple.com:2195 */
        $apnsServer = 'ssl://gateway.sandbox.push.apple.com:2195';
        /* Make sure this is set to the password that you set for your private key
        when you exported it to the .pem file using openssl on your OS X */
        $privateKeyPassword = '';
        /* Put your own message here if you want to */
        $message = "It's time to take your medication";
        
        /* Replace this with the name of the file that you have placed by your PHP
        script file, containing your private key and certificate that you generated
        earlier */
        $pushCertAndKeyPemFile = __DIR__.'/apns.pem';
        
        $stream = stream_context_create();
        // stream_context_set_option($stream,'ssl','passphrase',$privateKeyPassword);
        stream_context_set_option($stream,'ssl','local_cert',$pushCertAndKeyPemFile);
    
        $connectionTimeout = 20;
        $connectionType = STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT;
        $connection = stream_socket_client($apnsServer,$errorNumber,$errorString,$connectionTimeout,$connectionType,$stream);
    
        if (!$connection){
            echo "Failed to connect to the APNS server. Error no = $errorNumber<br/>";
            exit;
        } else {
            echo "Successfully connected to the APNS. Processing...</br>";
        }
        
        $messageBody['aps'] = array('alert' => $message,'sound' => 'default','badge' => 2,);
        $payload = json_encode($messageBody);
        
        $notification = chr(0) .
        pack('n', 32) .
        pack('H*', $deviceToken) .
        pack('n', strlen($payload)) .
        $payload;
    
        $wroteSuccessfully = fwrite($connection, $notification, strlen($notification));
        if (!$wroteSuccessfully){
            echo "Could not send the message<br/>";
        }
        else {
            echo "Successfully sent the message<br/>";
        }
        fclose($connection);
    }
?>