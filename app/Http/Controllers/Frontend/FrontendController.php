<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use GuzzleHttp\Client as GClient;
use Illuminate\Http\Request;
use IlluminateHttpRequest;
// use TwilioRestClient;
// use TwilioJwtAccessToken;
// use TwilioJwtGrantsVideoGrant;
use Twilio\Rest\Client;
use Twilio\Jwt\AccessToken;
use Twilio\Jwt\Grants\VideoGrant;
use App\Models\Communityusers;
use App\Models\Patients;
use App\Models\Tasks;

class FrontendController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    protected $sid;
    protected $token;
    protected $key;
    protected $secret;

    public function __construct()
    {
       $this->sid = config('services.twilio.sid');
       $this->token = config('services.twilio.token');
       $this->key = config('services.twilio.key');
       $this->secret = config('services.twilio.secret');
    }

    public function index()
    {
        $body_class = '';
        $user_id = auth()->user()->id;
        $patients = Communityusers::patientGroup(1);
        $tasks = Tasks::physicanTask($user_id);

        // $rooms = [];
        //    try {
        //        $client = new Client($this->sid, $this->token);
        //        $allRooms = $client->video->rooms->read([]);

        //         $rooms = array_map(function($room) {
        //            return $room->uniqueName;
        //         }, $allRooms);

        //    } catch (Exception $e) {
        //        echo "Error: " . $e->getMessage();
        //    }
        
        return view('frontend.index', compact('body_class', 'patients', 'tasks'));
    }

    public function createRoom(Request $request)
    {
        $body_class = '';
        $user_id = auth()->user()->id;
        $patients = Communityusers::patientGroup(1);
        $tasks = Tasks::physicanTask($user_id);

       $client = new Client($this->sid, $this->token);
       $exists = $client->video->rooms->read([ 'uniqueName' => $request->roomName]);

       if (empty($exists)) {
           $client->video->rooms->create([
               'uniqueName' => $request->roomName,
               'type' => 'group',
               'recordParticipantsOnConnect' => false
           ]);
       }
       
       $roomName = $request->roomName;
 
       $identity = auth()->user()->first_name;
       $token = new AccessToken($this->sid, $this->key, $this->secret, 3600, $identity);

       $videoGrant = new VideoGrant();
       $videoGrant->setRoom($roomName);

       $token->addGrant($videoGrant);
       $accessToken = $token->toJWT();
       return view('frontend.index', compact('body_class', 'accessToken', 'patients', 'tasks', 'roomName'));
    }

    public function patient_detail(Request $request) {
        $patient_id = $request->patientId;
        $patient_detail = Patients::find($patient_id);
        return $patient_detail;
    }

    public function sendSMS(Request $request) {
        $message  = $request->input('message');
        $to = $request->input('phoneNum');
         
        $twilioNumber = env('TWILIO_NUMBER');
        
        // $to = '312-273-8058';
        $twilio = new Client($this->sid, $this->token);
        $status = false;
        $result = '';
        // try {
        //     $client->messages->create(
        //         $to,
        //         [
        //             "body" => $message,
        //             "from" => $twilioNumber
        //         ]
        //     );
           
        //     $status = true;
        //     $result = 'Message sent to ' . $to;
        // } catch (TwilioException $e) {
        //     $result = 'Twilio replied with: ' . $e;
        // }
        $service = $twilio->chat->v2->services("ISXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX")
                            ->update(array(
                                         "notificationsAddedToChannelEnabled" => True,
                                         "notificationsAddedToChannelSound" => "default",
                                         "notificationsAddedToChannelTemplate" => "A New message from ${twilioNumber}: ${message}"
                                     )
                            );

        dd($service->friendlyName);
        return ['status'=>$status, 'result'=>$result];
    }
}
