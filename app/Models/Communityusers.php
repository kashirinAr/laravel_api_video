<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Communityusers extends Model
{
    //
    public static function patientGroup($doctor_id) {
    	return Communityusers::leftjoin('patients', 'patients.id', 'Communityusers.userid')->where('doctor_id', $doctor_id)->get()->toArray();
    }
}
