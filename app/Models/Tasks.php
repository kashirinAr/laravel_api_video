<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tasks extends Model
{
    //
    public static function physicanTask($physicanId) {
       return Tasks::leftjoin('task_types', 'task_types.id', 'tasks.type_id')->where('physician_id', $physicanId)->get();
    }
}
