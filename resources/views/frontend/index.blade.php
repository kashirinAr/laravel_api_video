 
@extends('frontend.layouts.frontend')

@section('title')
{{app_name()}}
@endsection
@section('style')
  <link rel="stylesheet" href="{{asset('css/frontend/calendar.css')}}" />
  @if(isset($accessToken))
  <script src="//media.twiliocdn.com/sdk/js/video/v1/twilio-video.min.js"></script>
<script>
	function participantConnected(participant) {
	   console.log('Participant "%s" connected', participant.identity);

	   const div = document.createElement('div');
	   div.id = participant.sid;
	   div.setAttribute("style", "float: left; margin: 10px;");
	   div.innerHTML = "<div style='clear:both'>"+participant.identity+"</div>";

	   participant.tracks.forEach(function(track) {
	       trackAdded(div, track)
	   });

	   participant.on('trackAdded', function(track) {
	       trackAdded(div, track)
	   });
	   participant.on('trackRemoved', trackRemoved);

	   document.getElementById('media-div').appendChild(div);
	}

	function participantDisconnected(participant) {
	   console.log('Participant "%s" disconnected', participant.identity);

	   participant.tracks.forEach(trackRemoved);
	   document.getElementById(participant.sid).remove();
	}

	function trackAdded(div, track) {
	   div.appendChild(track.attach());
	   var video = div.getElementsByTagName("video")[0];
	   if (video) {
	       video.setAttribute("style", "max-width:300px;");
	   }
	}

	function trackRemoved(track) {
	   track.detach().forEach( function(element) { element.remove() });
	}

    Twilio.Video.createLocalTracks({
       audio: true,
       video: { width: 300 }
    }).then(function(localTracks) {
       return Twilio.Video.connect('{{ $accessToken }}', {
           name: '{{ $roomName }}',
           tracks: localTracks,
           video: { width: 300 }
       });
    }).then(function(room) {
       console.log('Successfully joined a Room: ', room.name);

       room.participants.forEach(participantConnected);

       var previewContainer = document.getElementById(room.localParticipant.sid);
       if (!previewContainer || !previewContainer.querySelector('video')) {
           participantConnected(room.localParticipant);
       }

       room.on('participantConnected', function(participant) {
           console.log("Joining: '"+participant.identity+"'");
           participantConnected(participant);
       });

       room.on('participantDisconnected', function(participant) {
           console.log("Disconnected: '"+participant.identity+"'");
           participantDisconnected(participant);
       });
    });
    // additional functions will be added after this point
</script>
@endif
@endsection
@section('content')
  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-default elevation-4" style="background: white;">
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 mb-3 d-flex">
            <!-- SEARCH FORM -->
        <form class="form-inline ml-3">
          <div class="input-group input-group-sm frm">
            <input class="form-control form-control-navbar" type="search" placeholder="Patients Search" aria-label="Search" style="border: none; background: none;">
            <div class="input-group-append">
              <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
              </button>
            </div>
          </div>
        </form>
      </div>
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item has-treeview menu-open">
              <p class="ml-2" style="border-bottom: 1px solid ">
                Recent Patients
              </p>
          </li>
          @if(is_array($patients) && count($patients)>0)
          <select class="form-control" id="patientsList">
            <option></option>
            @foreach($patients as $patient)
            @if($patient['userId'] && $patient['first_name'])  
            <option value="{{$patient['userId']}}">
              {{isset($patient['first_name']) ? $patient['first_name'] : ''}} {{isset($patient['last_name']) ? $patient['last_name'] : ''}}
            </option>
            @endif
            @endforeach
          </select>
          @endif
          <br>
          <div id="current_patient"></div>
         
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" style="background: white;">
   <br/>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-4 col-md-12 mt-2">
            <!-- small box -->
            <div class="small-box box_shado bg-white">
              <div class="inner">
                <p class="text-center" style="color:#397EE3 !important;"> Most Recent </p>
               <center> <img src="{{asset('frontend/img/happy.png')}}" alt="HappyImage" class="img-fluid py-3" style="margin:-10px 0px;width: 50%;text-align:center;"></center>
                <p class="text-center mt-2"> I've excercised 3 days in a row so far</p>
              </div>
            </div>
             <div class="small-box box_shado bg-white">
              <div class="inner">
                <p class="text-center">Schedule Teleme Visit</p>
                <!-- <img src="{{asset('frontend/img/schedule.png')}}" alt="Schedule Time" class="img-fluid" style="-10px 0px;padding: 3px 18px;"> -->
                <div class="elegant-calencar">
       <p id="reset">reset</p>
        <div id="header" class="clearfix">
           <div class="pre-button"><</div>
            <div class="head-info">
                <div class="head-day"></div>
                <div class="head-month"></div>
            </div>
            <div class="next-button">></div>
        </div>
        <table id="calendar">
            <thead>
                <tr>
                    <th>Sun</th>
                    <th>Mon</th>
                    <th>Tue</th>
                    <th>Wed</th>
                    <th>Thu</th>
                    <th>Fri</th>
                    <th>Sat</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>

                <a href="#" class="btn btn-primary btn-block" style="margin: 18px 0px; border-radius: 30px;color:white !important;">
                 Set Appointment
                </a>
              </div>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-4 col-md-12">
            <!-- small box -->
            <div class="small-box customSmallBox bg-white ">
              <div class="inner">
                @if(!isset($accessToken))
                <center>
                  <img src="{{asset('frontend/img/mask.png')}}" alt="Mask Group" class="img-fluid">
                </center>
                @else
                <div class="content">
                  <div class="title m-b-md">
                      Video Chat Rooms
                  </div>

                  <div id="media-div">
                  </div>
              </div>
              @endif
              </div>
            </div>
          </div>
          <!-- ./col -->

         <!-- ./col -->
          <div class="col-lg-4 col-md-12">
            <!-- small box -->
            <p class="ml-2" style="border-bottom: 1px solid ">
                My tasks
              </p>
            <div class="small-box customSmallBox bg-white">
              <div class="inner">
                <!-- <img src="{{asset('frontend/img/goal.png')}}" alt="Mask Group" class="img-fluid box_shado b"> -->
                  <ul>
                    @foreach($tasks as $task)
                    <li>
                        {{ $task->name }}
                        <p>{{ $task->description }}</p>
                    </li>
                    @endforeach
                  </ul>
              </div>
            </div>
          </div>
          <!-- ./col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
  <div class="content">
   <div class="title m-b-md">
       Video Chat Rooms
   </div>
 
</div>

<form action="{{route('frontend.room.create')}}" method="post" id="room_create">
  {{csrf_field()}}
  <input type="hidden" id="userId" name="userId" />
  <input type="hidden" id="VroomName" name="roomName">
</form>

@endsection
@section('scripts')
<script src="{{asset('js/frontend/calendar.js')}}"></script>
<script>
  $(document).on('click', '.startVideoCall', function(){
     var submitForm = $('#room_create')
     submitForm.submit()
  })
</script>
<script>
  $('#patientsList').change(function() {
     var patientId = $(this).val()
     // var message  = $('#sms_msg').text()
     $.ajax({
       type:'POST',
       url:"{{ route('frontend.patient.detail')}}",
       data:{'patientId': patientId },
       success:function(data){
         // if(data.status) {
          $('#userId').val(data.id)
          $('#VroomName').val('TESTROOM-'+data.id)
          var patientEmail = data.email;
          var patientFirstName = data.first_name;
          var patientLastName = data.last_name;
          var patientPhone = data.phone_number;
          var patientId = data.id;
          var html = '<li class="PatientsName">'
          html += '<p class="mari">'+patientFirstName+' '+patientLastName+'</p>'
          html += '<p> Patient Phone: '+patientPhone+'</p>'
          html += '<p> Patient ID: 393H-R44H-'+patientId+'</p>'
          html += '<div class="row"><div class="col-md-6"><button class="btn btn-primary PatientAppointmentBtn startVideoCall" id="'+patientId+'">'
          html += 'Create An Appointment</button></div>'
          // <div class="col-md-6">'
          // html += '<button class="PatientSmsBtn PatientAppointmentBtn" id="p-'+patientId+'" data-phone="'+patientPhone+'">'
          // html += '<i class="fas fa-tv"></i>Send Notification</button></div></div>'
          html += '</li>'
          $('#current_patient').html(html)
       }
     })
  })
</script>
<script type="text/javascript">
    // $(function () {
    //     $('#appointmentCalendar').datetimepicker({
    //         inline: true,
    //         sideBySide: true
    //     });
    // });
</script>
@endsection
