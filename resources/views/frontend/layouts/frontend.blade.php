<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('img/favicon.png')}}">
    <link rel="icon" type="image/png" href="{{asset('img/favicon.png')}}">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0, shrink-to-fit=no' name='viewport' />
    <!-- Shortcut Icon -->
    <link rel="shortcut icon" href="{{asset('img/favicon.png')}}">
    <link rel="icon" type="image/ico" href="{{asset('img/favicon.png')}}" />

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <script src="{{asset('frontend/plug/jquery.min.js')}}"></script>
     @stack('before-styles')
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="{{asset('frontend/css/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/core/adminlte.min.css')}}">
    <link rel="stylesheet" href="{{asset('frontend/css/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
    @yield('style')
</head>

<body class="{{isset($body_class) ? $body_class : ''}} hold-transition sidebar-mini layout-fixed">
    
<div class="wrapper">

<nav class="main-header navbar navbar-expand navbar-white navbar-light">
  <!-- Left navbar links -->
  <a class="nav-link menu_I" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
  <div class="logo_img"><a href="/"><img src="{{asset('frontend/img/Logo.png')}}" alt="" /></a></div>
  <ul class="navbar-nav res_menu">
    <li class="nav-item mnav d-sm-inline-block">
      <a href="/" class="nav-link">HOME</a>
    </li>
    <li class="nav-item mnav d-sm-inline-block">
      <a href="#" class="nav-link">PATIENTS</a>
    </li>
    <li class="nav-item mnav d-sm-inline-block">
      <a href="#" class="nav-link">SETUP</a>
    </li>
  </ul>


  <!-- Right navbar links -->
  <ul class="navbar-nav ml-auto">
    <li class="nav-item dropdown" style="margin-top: -13px;" id="infodr">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <p class="wadel"> Dr Wadel Janes <br/>
          <span class="hospital" style="font-size: 10px;"> 
            NorthWestern Memorial Hospital
          </span> <br/>
          <span class="dibates" style="font-size: 8px;">
            Dibates Pill Trial
          </span>
        </p>
       </a>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link" data-toggle="dropdown" href="#">
        <img src="{{asset('frontend/img/avatar.png')}}" alt="avatar" class="avtr">
        <span class="badge navbar-badge">
          <img src="{{asset('frontend/img/arrow.png')}}" alt="Arrow Image">
        </span>
      </a>
      <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
        <a href="#" class="dropdown-item">
          <!-- Message Start -->
          <div class="media">
            <div class="media-body">
              <h3 class="dropdown-item-title text-center">
                Edit Profile
                <span class="float-right text-sm text-danger">
              </h3>
            </div>
          </div>
          <!-- Message End -->
        </a>
        <div class="dropdown-divider"></div>
        <a href="#" class="dropdown-item">
          <!-- Message Start -->
          <div class="media">
            <div class="media-body">
              <h3 class="dropdown-item-title text-center">
                Edit Profile
                <span class="float-right text-sm text-danger">
              </h3>
            </div>
          </div>
          <!-- Message End -->
        </a>
        <div class="dropdown-divider"></div> 
        <a href="#" class="dropdown-item dropdown-footer">Logout</a>
      </div>
    </li>
  </ul>
</nav>
<!-- /.navbar -->
@yield('content')
</div>

</body>

<!-- Scripts -->
@stack('before-scripts')
<!-- jQuery -->
<!-- jQuery UI 1.11.4 -->
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<!-- Bootstrap 4 -->
<script src="{{asset('frontend/plug/bootstrap.bundle.min.js')}}"></script>
<script>
  // $.widget.bridge('uibutton', $.ui.button)
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
</script>
<script src="{{asset('frontend/dist/js/adminlte.js')}}"></script>

@stack('after-scripts')
@yield('scripts')

</html>
